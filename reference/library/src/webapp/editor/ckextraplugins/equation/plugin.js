/* 
 Copyright CodeCogs 2006-2011
 Written by Will Bateman.
 
 Version 1: CK Editor Plugin that insert LaTeX into HTML
*/



/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

var currentEdit=null; 

(function()
{
	var popupEqnwin=null;
  var createEqnDefinition = 
	{
		preserveState:true,
		editorFocus:false,
		exec : function(editor, latex)
		{
			currentEdit=CKEDITOR.currentInstance;
			
			//open a popup window when the button is clicked
			if (popupEqnwin==null || popupEqnwin.closed || !popupEqnwin.location) 
			{
				var url='/library/editor/ckextraplugins/equation/editor/editor.html';
		
				//if(language!='') url+='&lang='+language;
				if(latex!==undefined) 
				{	
					latex=unescape(latex);
					latex=latex.replace(/\+/g,'&plus;');
					url+='&latex='+escape(latex);
				}
				
				popupEqnwin=window.open(url,'LaTexEditor','width=700,height=450,status=1,scrollbars=yes,resizable=1');
				if (!popupEqnwin.opener) popupEqnwin.opener = self;
			}
			else if (window.focus) 
			{ 
				popupEqnwin.focus()
				if(latex!==undefined)
				{
					latex=unescape(latex);
	
					try
					{
						popupEqnwin.EqEditor.load(latex);
					}
					catch(err)
					{
						alert(err.message);
					}
				}
				popupEqnwin.document.getElementById("latex_formula").focus();
				popupEqnwin.document.getElementById("latex_formula").select();
			}
		}
	};

	CKEDITOR.plugins.add( 'equation',
	{
	  lang : ['en'],
		
		init : function( editor )
		{
			var com="equation";
			
			// Add the link and unlink buttons.
			editor.addCommand( com, createEqnDefinition);
							
			editor.ui.addButton( 'equation',
				{
					label : editor.lang.equation.title,
					command : com,
					icon: this.path + 'images/equation.gif'
				});
	
			// If the "menu" plugin is loaded, register the menu items.
			if ( editor.addMenuItems )
			{
				editor.addMenuItems(
					{
						equation :
						{
							label : 'Edit Equation',
							command : 'equation',
							group : 'equation'
						}
					});
			}	
			
			editor.on( 'doubleclick', function(evt) 
			  {
					var element = evt.data.element;
					if (element && element.is('img')) 
					{
				  	var sName = element.getAttribute('src').match( /(gif|svg)\.latex\?(.*)/ );
				 	  if(sName!=null)
						{
							createEqnDefinition.exec(null, sName[2]);	
							evt.cancelBubble = true; 
              evt.returnValue = false;
	            evt.stop();	
						}
			    }
				}, null, null, 1);
	
		}
		

	});
	
	
	// Add a new placeholder at the actual selection.
	CKEditor_Add = function( name )
	{
			currentEdit=CKEDITOR.currentInstance;
	
		var image = new Image();
	
		var sName = name.match( /\\f([\[\$])(.*?)\\f[\]\$]/ );
		var eq='';
		var imgstyle = "cursor: default;";

		eq = escape(sName[2].replace(/\+/g,'&plus;'));
		if(sName[1]=='$')
		{       eq='\\inline '+eq; }
		else
		{
			imgstyle += "display: block;";
			imgstyle += "margin: 0 auto;";
			imgstyle += "textalign: center;";
		}

		image.src = '/cgi-bin/latex.cgi?'+eq;
		image.align='absmiddle';
		image.alt=name;
		for(var name in CKEDITOR.instances){
			CKEDITOR.instances[name].focus();
			CKEDITOR.instances[name].insertHtml('<img src="'+image.src+'" alt="'+image.alt+'" align="'+image.align+'" style="'+imgstyle+'"/>');
		}
	}
})();	
