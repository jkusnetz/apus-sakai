<?xml version="1.0" encoding="UTF-8"?>
<!-- 
   Sakai Configuration Viewer tool project.xml - created by duhrer@gmail.com

   Copyright (c) 2007 Virginia Polytechnic Institute and State University
   Licensed under the Educational Community License version 1.0
   
   A copy of the Educational Community License has been included in this 
   distribution and is available at: http://www.opensource.org/licenses/ecl1.php
   
   Contributors:
   Anthony Atkins (duhrer@gmail.com)
-->

<project>
	<pomVersion>3</pomVersion>
	<extend>../../master/project.xml</extend>
	<name>sakai-configviewer-tool</name>
	<groupId>sakaiproject</groupId>
	<id>sakai-configviewer-tool</id>
	<currentVersion>${sakai.version}</currentVersion>
	<organization>
		<name>Sakai Project</name>
		<url>http://www.sakaiproject.org/</url>
	</organization>
	<inceptionYear>2006</inceptionYear>

	<!-- You must deploy your tool as a war file -->
	<properties>
		<deploy.type>war</deploy.type>
	</properties>

	<dependencies>

		<!-- The sakaiproject plugin let's us build our tool without building
			the entire sakai code base -->
		<dependency>
			<groupId>sakaiproject</groupId>
			<artifactId>sakai</artifactId>
			<version>${sakai.plugin.version}</version>
			<type>plugin</type>
		</dependency>

        <!-- internal dependencies -->
        <dependency>
            <groupId>sakaiproject</groupId>
            <artifactId>sakai-configviewer-model-api</artifactId>
            <version>${sakai.version}</version>
        </dependency>

        <dependency>
            <groupId>sakaiproject</groupId>
            <artifactId>sakai-configviewer-logic-api</artifactId>
            <version>${sakai.version}</version>
        </dependency>

		<!-- Servlet dependency -->
		<dependency>
			<groupId>servletapi</groupId>
			<artifactId>servletapi</artifactId>
			<version>2.4</version>
		</dependency>

		<!-- Apache commons dependencies -->
		<dependency>
			<groupId>commons-logging</groupId>
			<artifactId>commons-logging</artifactId>
			<version>1.0.4</version>
		</dependency>

		<!-- Spring dependency -->
		<dependency>
			<groupId>${sakai.spring.groupId}</groupId>
			<artifactId>${sakai.spring.artifactId}</artifactId>
			<version>${sakai.spring.version}</version>
		</dependency>

		<!-- Sakai dependencies (all RSF apps must have this) -->
		<dependency>
			<groupId>sakaiproject</groupId>
			<artifactId>sakai-util</artifactId>
			<version>${sakai.version}</version>
			<properties>
				<war.bundle>true</war.bundle>
			</properties>
		</dependency>
				<dependency>
			<groupId>sakaiproject</groupId>
			<artifactId>sakai-component-api</artifactId>
			<version>${sakai.version}</version>
		</dependency>

		<!-- RSF dependencies -->
		<dependency>
			<groupId>ponderutilcore</groupId>
			<artifactId>ponderutilcore</artifactId>
			<!--  only used for development
			<version>${ponderutilcore.version}</version>
			-->
			<version>1.2.2</version>
			<type>jar</type>
			<properties>
				<war.bundle>true</war.bundle>
			</properties>
		</dependency>

		<dependency>
			<groupId>j-servletutil</groupId>
			<artifactId>j-servletutil</artifactId>
			<!--  only used for development
			<version>${jservletutil.version}</version>
			-->
			<version>1.2.1</version>
			<type>jar</type>
			<properties>
				<war.bundle>true</war.bundle>
			</properties>
		</dependency>

		<dependency>
			<groupId>rsfutil</groupId>
			<artifactId>rsfutil</artifactId>
			<!-- only used for development  
			<version>${rsfutil.version}</version>
			-->
			<version>0.7.2</version>
			<type>jar</type>
			<properties>
				<war.bundle>true</war.bundle>
			</properties>
		</dependency>

		<dependency>
			<groupId>sakairsf</groupId>
			<artifactId>sakairsf</artifactId>
			<!--  only used for development
			<version>${rsfutil.version}-sakai_${sakai.version}</version>
			-->
			<version>0.7.2-sakai_2.2.x</version>
			<type>jar</type>
			<properties>
				<war.bundle>true</war.bundle>
			</properties>
		</dependency>

		<!-- Aleksander Slominski's (U Indiana) lightning-fast XML pull parser -->
		<dependency>
			<groupId>xpp3</groupId>
			<artifactId>xpp3</artifactId>
			<version>1.1.3.4-RC8_min</version>
			<type>jar</type>
			<properties>
				<war.bundle>true</war.bundle>
			</properties>
		</dependency>
	</dependencies>
	
	<build>
		<sourceDirectory>src/java</sourceDirectory>
		<resources>
			<resource>
				<directory>${basedir}/src/java</directory>
				<includes>
					<include>**/*.xml</include>
					<include>**/*.properties</include>
				</includes>
			</resource>
		</resources>
	</build>

</project>

