/******************************************************************************
 * ConfigViewerItemBean.java - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 Virginia Polytechnic Institute and State University
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/

package org.sakaiproject.configviewer.tool;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sakaiproject.configviewer.logic.ConfigViewerLogic;
import org.sakaiproject.util.ResourceLoader;

import uk.org.ponder.stringutil.StringList;

/**
 * This is the backing bean for actions related to SampleCrudToolItems
 * @author Sakai App Builder -AZ
 */
public class ConfigViewerItemBean {
	
	private static Log log = LogFactory.getLog(ConfigViewerItemBean.class);

	public int startRecord = 1;
	public int recordsPerPage = 25;
	public String filterText = "";
	
	private ConfigViewerLogic logic;
	public void setLogic(ConfigViewerLogic logic) {
		this.logic = logic;
	}
	
	private StringList messages;
	public void setMessages(StringList messages) {
		this.messages = messages;
	}

	public void init() throws Exception {
		log.debug("init");
	}

	public ConfigViewerItemBean() {
		log.debug("constructor");
	}
}

