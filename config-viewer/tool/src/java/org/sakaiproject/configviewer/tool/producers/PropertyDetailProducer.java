/******************************************************************************
 * PropertyDetailProducer.java - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 UHI Millenium Institute
 * Copyright (c) 2007,2006 Virginia Polytechnic Institute and State University
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/

package org.sakaiproject.configviewer.tool.producers;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.sakaiproject.component.cover.ServerConfigurationService;
import org.sakaiproject.configviewer.logic.ConfigViewerLogic;
import org.sakaiproject.configviewer.model.ConfigViewerItem;
import org.sakaiproject.configviewer.tool.ConfigViewerItemBean;
import org.sakaiproject.configviewer.tool.params.PropertyDetailParameters;

import uk.org.ponder.rsf.components.UIBranchContainer;
import uk.org.ponder.rsf.components.UIContainer;
import uk.org.ponder.rsf.components.UIMessage;
import uk.org.ponder.rsf.components.UIOutput;
import uk.org.ponder.rsf.components.UIVerbatim;
import uk.org.ponder.rsf.components.decorators.DecoratorList;
import uk.org.ponder.rsf.components.decorators.UIFreeAttributeDecorator;
import uk.org.ponder.rsf.view.ComponentChecker;
import uk.org.ponder.rsf.view.ViewComponentProducer;
import uk.org.ponder.rsf.viewstate.ViewParameters;
import uk.org.ponder.rsf.viewstate.ViewParamsReporter;

/**
 * This is the view producer for the Config Viewer Tool
 * @author Anthony Atkins (duhrer@gmail.com)
 */
public class PropertyDetailProducer implements ViewComponentProducer, ViewParamsReporter {
	// the max width of the value, help, and tool strings, in characters
	public static final int MAX_WIDTH = 40;
	
	// The VIEW_ID must match the html template (without the .html)
	public static final String VIEW_ID = "PropertyDetail"; 
	
	public String getViewID() {
		return VIEW_ID;
	}

	private ConfigViewerLogic logic;

	/**
	 * Function to allow Spring to inject the logic bean.
	 * @param logic
	 */
	public void setLogic(ConfigViewerLogic logic) {
		this.logic = logic;
	}

	private ServerConfigurationService serverConfigurationService;
	public void setServerConfigurationService(
			ServerConfigurationService serverConfigurationService) {
		this.serverConfigurationService = serverConfigurationService;
	}
	

  /**
   * Function to allow Spring to inject the Locale. 
   * @param Locale
   */
  private Locale locale;
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

/**
 * Fill the RSF components to make the display for the main Config Viewer page
 * @param tofill The UIContainer to fill
 * @param viewparams The current ViewParameters
 * @param checker The current ComponentChecker
 */	
	public void fillComponents(UIContainer tofill, ViewParameters viewparams,
			ComponentChecker checker) {		

		// make absolutely sure that only admins can view this information
		if (! this.logic.isAdmin()) {
			//throw new SecurityException(messageLocator.getMessage("cv.admin.notice")); 
			// this should apparently use a Bean Guard, which I'm sure Antranig will try to explain to me unsuccessfully and end up writing by proxy.
			throw new SecurityException("You must be an admin to use this tool."); 
		}
		
		// use a date which is related to the current users locale
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);
		
		PropertyDetailParameters pdp = (PropertyDetailParameters) viewparams;

		// The text inside the page's <title> tags
		UIMessage.make(tofill,"config-viewer-properties-page-title","pd.properties.pagetitle");  

		// The title of the properties viewer tool
		UIMessage.make(tofill,"config-viewer-properties-title","pd.properties.title");  
		
		// The headers for the data row
		UIMessage.make(tofill, "config-viewer-name-header", "cv.header.name");  
		UIMessage.make(tofill, "config-viewer-value-header", "cv.header.value");  
		UIMessage.make(tofill, "config-viewer-help-header", "cv.header.help");  
		UIMessage.make(tofill, "config-viewer-syntax-header", "cv.header.syntax");  
		UIMessage.make(tofill, "config-viewer-tools-header", "cv.header.tools");  
		UIMessage.make(tofill, "config-viewer-version-header", "cv.header.version");  

		// the text that appears in front of the current date
		UIMessage.make(tofill, "config-viewer-generated-on-text", "cv.generatedon");  
		UIOutput.make(tofill, "current-date", df.format(new Date()));
		
		ConfigViewerItem item = (ConfigViewerItem) this.logic.getItem(pdp.propertyKey);
		
		UIBranchContainer configEntry = UIBranchContainer.make(tofill, "config-entry:", item.getId() ); 
		
		UIOutput.make(configEntry, "config-entry-name", item.getId()); 

		if (item.getType().equals("getStrings")) {
			String[] currentValueList = serverConfigurationService.getStrings(item.getId());
			if (currentValueList != null) {
				for (int d=0; d< currentValueList.length; d++) {
					UIBranchContainer configEntryValueBranch = UIBranchContainer.make(configEntry,"config-entry-value-branch:", "cevb_0"+"_"+d); 

					if ("html".equals(item.getRule())) {
						UIVerbatim.make(configEntryValueBranch, "config-entry-value", currentValueList[d]);
					}
					else {
						// if the property value is too large, truncate the display and generate a "more" link with the full text
						UIOutput.make(configEntryValueBranch, "config-entry-value", currentValueList[d]);											 
					}
				}
			}

			UIBranchContainer configEntrySyntaxBranch =  UIBranchContainer.make(configEntry, "config-entry-syntax-branch:","cesb_0"); 
			UIMessage.make(configEntrySyntaxBranch, "config-entry-syntax", "syntax_getstrings", new Object[] {item.getId(), item.getQualifier()}); 
		}
		else {
			String currentValue = serverConfigurationService.getString(item.getId());
			
			// only need one branch container for strings 
			UIBranchContainer configEntryValueBranch = UIBranchContainer.make(configEntry,"config-entry-value-branch:", "cevb_0"); 

			// only display values that are set
			if (currentValue.length() > 0) {
				// redact any password information
				if (item.getId().matches(".*password.*")) { 
					UIOutput.make(configEntryValueBranch, "config-entry-value", "*********");				  
				}
				else if ("html".equals(item.getRule())) {
					UIVerbatim.make(configEntryValueBranch, "config-entry-value", currentValue);
				}
				else {
					UIOutput.make(configEntryValueBranch, "config-entry-value", currentValue);				 
				}
			}
			else {
				UIMessage configEntryValue = UIMessage.make(configEntryValueBranch, "config-entry-value", "cv.unset");	  
				Map<String, String> greyTextAttrmap = new HashMap<String, String>();
				greyTextAttrmap.put("style", "font-style:italic;color:#999999;");  
				configEntryValue.decorators = new DecoratorList(new UIFreeAttributeDecorator(greyTextAttrmap)); 
			}

			UIBranchContainer configEntrySyntaxBranch =  UIBranchContainer.make(configEntry, "config-entry-syntax-branch:","cesb_0"); 
			UIMessage.make(configEntrySyntaxBranch, "config-entry-syntax", "syntax_"+item.getRule(), new Object[] {item.getQualifier()}); 
		}
		
		UIBranchContainer configEntryHelpBranch =  UIBranchContainer.make(configEntry, "config-entry-help-branch:", "cehb_0"); 
		UIMessage.make(configEntryHelpBranch, "config-entry-help", item.getId()); 


		UIBranchContainer configEntryToolBranch =  UIBranchContainer.make(configEntry, "config-entry-tool-branch:", "cetb_0"); 
		UIOutput.make(configEntryToolBranch, "config-entry-tool", item.getTool()); 

		UIOutput.make(configEntry, "config-entry-version", item.getVersion()); 
		
		// Make a link to close the window/tab
		UIMessage.make(tofill, "config-viewer-window-close", "pd.closewindow");	
	}
	

	public ViewParameters getViewParameters() {
		return new PropertyDetailParameters(VIEW_ID,"");
	}

}
