/******************************************************************************
 * PropertyDetailParameters.java - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 Virginia Polytechnic Institute and State University
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/

package org.sakaiproject.configviewer.tool.params;

import uk.org.ponder.rsf.viewstate.SimpleViewParameters;

/**
 * This is a view parameters class which defines the variables that are
 * passed from one page to another
 * @author Anthony Atkins (duhrer@gmail.com)
 */
public class PropertyDetailParameters extends SimpleViewParameters {
	public String propertyKey; // the identifier for the property to be returned

	/**
	 * Basic empty constructor
	 */
	public PropertyDetailParameters() {
	}

	/**
	 * Constructor that uses everything but the filter text.
	 * @param viewId A valid RSF View ID.
	 * @param propertyKey the unique identifier (ex: auto.ddl) for the property to be viewed
	 */
	public PropertyDetailParameters(String viewId, String propertyKey) {
		this.viewID=viewId;
		this.propertyKey=propertyKey;
	}

	/* (non-Javadoc)
	 * @see uk.org.ponder.rsf.viewstate.ViewParameters#getParseSpec()
	 */
	public String getParseSpec() {
		// include a comma delimited list of the public properties in this class
		return super.getParseSpec() + ",propertyKey";
	}
}
