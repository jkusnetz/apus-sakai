/******************************************************************************
 * ConfigViewerParameters.java - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 Virginia Polytechnic Institute and State University
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/

package org.sakaiproject.configviewer.tool.params;

import org.sakaiproject.configviewer.tool.ConfigViewerItemBean;

import uk.org.ponder.rsf.viewstate.SimpleViewParameters;

/**
 * This is a view parameters class which defines the variables that are
 * passed from one page to another
 * @author Anthony Atkins (duhrer@gmail.com)
 */
public class ConfigViewerParameters extends SimpleViewParameters {

	public int startRecord; // the record number to start with
	public int recordsPerPage; // the number of records per page
	public String filterText; // the text to filter the display by

	/**
	 * Basic empty constructor
	 */
	public ConfigViewerParameters() {
	}

	/**
	 * Constructor that uses everything but the filter text.
	 * @param viewId A valid RSF View ID.
	 * @param startRecord The first record to display.
	 * @param recordsPerPage The number of records per page to display.
	 */
	public ConfigViewerParameters(String viewId, int startRecord, int recordsPerPage) {
		this.viewID=viewId;
		this.startRecord = startRecord;
		this.recordsPerPage = recordsPerPage;
	}

	/**
	 * Constructor that fills in everything including the filter text.
	 * @param viewId A Valid RSF View ID.
	 * @param startRecord The first record to display.
	 * @param recordsPerPage The number of records per page to display.
	 * @param filterText The filter text (search string) to limit the display by.
	 */
	public ConfigViewerParameters(String viewId, int startRecord, int recordsPerPage, String filterText) {
		this.viewID=viewId;
		this.startRecord = startRecord;
		this.recordsPerPage = recordsPerPage;
		this.filterText = filterText;
	}

	private ConfigViewerItemBean configViewerItemBean;
	
	/**
	 * Function to allow Spring to inject the tool bean.
	 * @param logic
	 */
	public void setConfigViewerItemBean(ConfigViewerItemBean configViewerItemBean) {
		this.configViewerItemBean = configViewerItemBean;
	}
	
	/* (non-Javadoc)
	 * @see uk.org.ponder.rsf.viewstate.ViewParameters#getParseSpec()
	 */
	public String getParseSpec() {
		// include a comma delimited list of the public properties in this class
		return super.getParseSpec() + ",startRecord,recordsPerPage,filterText";
	}
}
