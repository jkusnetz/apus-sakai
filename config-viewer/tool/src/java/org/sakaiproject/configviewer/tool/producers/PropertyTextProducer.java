/******************************************************************************
 * PropertyTextProducer.java - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 UHI Millenium Institute
 * Copyright (c) 2007,2006 Virginia Polytechnic Institute and State University
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/

package org.sakaiproject.configviewer.tool.producers;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.sakaiproject.component.cover.ServerConfigurationService;
import org.sakaiproject.configviewer.logic.ConfigViewerLogic;
import org.sakaiproject.configviewer.model.ConfigViewerItem;
import org.sakaiproject.configviewer.tool.ConfigViewerItemBean;
import org.sakaiproject.configviewer.tool.params.ConfigViewerParameters;
import org.sakaiproject.configviewer.tool.params.PropertyTextParameters;
import org.sakaiproject.util.ResourceLoader;

import uk.org.ponder.rsf.components.UIBranchContainer;
import uk.org.ponder.rsf.components.UIContainer;
import uk.org.ponder.rsf.components.UIInternalLink;
import uk.org.ponder.rsf.components.UIMessage;
import uk.org.ponder.rsf.components.UIOutput;
import uk.org.ponder.rsf.components.UIVerbatim;
import uk.org.ponder.rsf.view.ComponentChecker;
import uk.org.ponder.rsf.view.ViewComponentProducer;
import uk.org.ponder.rsf.viewstate.ViewParameters;
import uk.org.ponder.rsf.viewstate.ViewParamsReporter;

/**
 * This producer displays text suitable for outputting to a sakai.properties file.
 * @author Anthony Atkins (duhrer@gmail.com)
 */
public class PropertyTextProducer implements ViewComponentProducer, ViewParamsReporter {
	// the max width of the comment text, in characters
	public static final int MAX_WIDTH = 100;
	
	// The VIEW_ID must match the html template (without the .html)
	public static final String VIEW_ID = "PropertyText"; 
	
	public String getViewID() {
		return VIEW_ID;
	}

	private ConfigViewerLogic logic;

	/**
	 * Function to allow Spring to inject the logic bean.
	 * @param logic
	 */
	public void setLogic(ConfigViewerLogic logic) {
		this.logic = logic;
	}

	private ServerConfigurationService serverConfigurationService;
	public void setServerConfigurationService(
			ServerConfigurationService serverConfigurationService) {
		this.serverConfigurationService = serverConfigurationService;
	}
	
//	private ConfigViewerItemBean configViewerItemBean;
//	/**
//	 * Function to allow Spring to inject the configViewerItemBean.
//	 * @param configViewerItemBean
//	 */
//	public void setConfigViewerItemBean(ConfigViewerItemBean configViewerItemBean) {
//		this.configViewerItemBean = configViewerItemBean;
//	}

  /**
   * Function to allow Spring to inject the Locale. 
   * @param Locale
   */
  private Locale locale;
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

/**
 * Fill the RSF components to make the display for the main Config Viewer page
 * @param tofill The UIContainer to fill
 * @param viewparams The current ViewParameters
 * @param checker The current ComponentChecker
 */	
	public void fillComponents(UIContainer tofill, ViewParameters viewparams,
			ComponentChecker checker) {		

		ResourceLoader rb = new ResourceLoader("org.sakaiproject.configviewer.propertiesHelpMessages");
		
		// make absolutely sure that only admins can view this information
		if (! this.logic.isAdmin()) {
			//throw new SecurityException(messageLocator.getMessage("cv.admin.notice")); 
			// this should apparently use a Bean Guard, which I'm sure Antranig will try to explain to me unsuccessfully and end up writing by proxy.
			throw new SecurityException("You must be an admin to use this tool."); 
		}
		
		// The text inside the page's <title> tags
		UIMessage.make(tofill,"config-viewer-text-page-title","text.properties.pagetitle");  

		// The title of the properties viewer tool
		UIMessage.make(tofill,"config-viewer-text-title","text.properties.title");  

		// The links between sections of the tool
		UIInternalLink.make(tofill, "config-viewer-properties-link", UIMessage.make("cv.properties.link"), new ConfigViewerParameters()); 
		UIMessage.make(tofill, "config-viewer-text-link", "cv.text.link");

		// A descriptive introduction for the page
		UIMessage.make(tofill,"config-viewer-text-intro", "text.properties.intro");
		
		// use a date which is related to the current users locale
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);
		// the text that appears in front of the current date
		UIMessage.make(tofill, "config-viewer-text-generated-on", "text.generatedon");  
		UIOutput.make(tofill, "config-viewer-text-current-date", df.format(new Date()));
		
		List l;
		l = logic.getAllItems();

		for (int j=0; j < l.size(); j ++) {
			ConfigViewerItem item = (ConfigViewerItem) l.get(j);
			UIBranchContainer textEntry = UIBranchContainer.make(tofill, "config-viewer-text-entry:", "cvte"+ j);
			// fill in the comment starting with the item being filled in
			
			// The first line is always the property ID
			String commentString = "#\n# " + item.getId() + ":\n";

			String rawString = rb.getString(item.getId());

			// wrap the rest of the text and add comment characters
			if (rawString.length() > (MAX_WIDTH - 4)) {
				while (rawString.length() > (MAX_WIDTH - 4)) {
					if (rawString.substring(0, MAX_WIDTH - 4).contains(" ")) {
						commentString += "#   " + rawString.substring(0,rawString.substring(0, MAX_WIDTH - 4 ).lastIndexOf(" ")) + "\n";
						rawString = rawString.substring(rawString.substring(0, MAX_WIDTH - 4).lastIndexOf(" "));
					}
					else {
						commentString += "#   " + rawString.substring(0,MAX_WIDTH - 4) + "- " + "\n";
						rawString = rawString.substring(0, MAX_WIDTH -4);
					}
				}
			}

			commentString += "#   " + rawString + "\n";

			UIVerbatim.make(textEntry, "config-viewer-text-comment", commentString);

			if (item.getType().equals("getStrings")) {
				String[] currentValueList = serverConfigurationService.getStrings(item.getId());
				if (currentValueList != null) {
					// output multiple entries for each array
					for (int d=0; d< currentValueList.length; d++) {
						UIBranchContainer textEntryValueBranch = UIBranchContainer.make(textEntry, "config-viewer-text-value-entry:","cevt_row_" + d);
						UIOutput.make(textEntryValueBranch, "config-viewer-text-value", item.getId() + "." + (d+1) + "=" + currentValueList[d] + "\n");
					}					

					UIBranchContainer textEntryValueBranch = UIBranchContainer.make(textEntry, "config-viewer-text-value-entry:","cevt_row_count");
					UIOutput.make(textEntryValueBranch, "config-viewer-text-value", item.getId()+".count=" + currentValueList.length + "\n");

				}
			}
			else {
				String currentValue = serverConfigurationService.getString(item.getId());
				// fill in the value starting with the item and an equals sign
				UIBranchContainer textEntryValueBranch = UIBranchContainer.make(textEntry, "config-viewer-text-value-entry:","cevt_row_0");
				UIOutput.make(textEntryValueBranch, "config-viewer-text-value", item.getId()+"="+currentValue);
			}
		}
	}

	public ViewParameters getViewParameters() {
		return new PropertyTextParameters(VIEW_ID);
	}

}
