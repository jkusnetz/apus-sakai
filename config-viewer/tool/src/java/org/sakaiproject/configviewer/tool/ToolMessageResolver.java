package org.sakaiproject.configviewer.tool;

import org.sakaiproject.configviewer.logic.MessageResolver;
import org.sakaiproject.util.ResourceLoader;

public class ToolMessageResolver implements MessageResolver {
	public String resolveMessage(String key) {
		ResourceLoader rb = new ResourceLoader("org.sakaiproject.configviewer.propertiesHelpMessages");
		return rb.getString(key);
	}
}
