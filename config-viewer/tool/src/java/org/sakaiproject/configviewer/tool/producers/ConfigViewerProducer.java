/******************************************************************************
 * ConfigViewerProducer.java - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 Virginia Polytechnic Institute and State University
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/

package org.sakaiproject.configviewer.tool.producers;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.sakaiproject.component.cover.ServerConfigurationService;
import org.sakaiproject.configviewer.logic.ConfigViewerLogic;
import org.sakaiproject.configviewer.model.ConfigViewerItem;
import org.sakaiproject.configviewer.tool.ConfigViewerItemBean;
import org.sakaiproject.configviewer.tool.ToolMessageResolver;
import org.sakaiproject.configviewer.tool.params.ConfigViewerParameters;
import org.sakaiproject.configviewer.tool.params.PropertyDetailParameters;
import org.sakaiproject.configviewer.tool.params.PropertyTextParameters;
import org.sakaiproject.util.ResourceLoader;

import uk.org.ponder.rsf.components.UIBranchContainer;
import uk.org.ponder.rsf.components.UIContainer;
import uk.org.ponder.rsf.components.UIForm;
import uk.org.ponder.rsf.components.UIInput;
import uk.org.ponder.rsf.components.UIInternalLink;
import uk.org.ponder.rsf.components.UILink;
import uk.org.ponder.rsf.components.UIMessage;
import uk.org.ponder.rsf.components.UIOutput;
import uk.org.ponder.rsf.components.UISelect;
import uk.org.ponder.rsf.components.UIVerbatim;
import uk.org.ponder.rsf.components.decorators.DecoratorList;
import uk.org.ponder.rsf.components.decorators.UIFreeAttributeDecorator;
import uk.org.ponder.rsf.view.ComponentChecker;
import uk.org.ponder.rsf.view.DefaultView;
import uk.org.ponder.rsf.view.ViewComponentProducer;
import uk.org.ponder.rsf.viewstate.ViewParameters;
import uk.org.ponder.rsf.viewstate.ViewParamsReporter;

/**
 * This is the view producer for the Config Viewer Tool
 * @author Anthony Atkins (duhrer@gmail.com)
 */
public class ConfigViewerProducer implements ViewComponentProducer, ViewParamsReporter, DefaultView {
	// the max width of the value, help, syntax, and tool strings, in characters
	public static final int MAX_WIDTH = 40;
	
	// The VIEW_ID must match the html template (without the .html)
	public static final String VIEW_ID = "ConfigViewer"; 
	
	public String getViewID() {
		return VIEW_ID;
	}

	private ConfigViewerLogic logic;

	/**
	 * Function to allow Spring to inject the logic bean.
	 * @param logic
	 */
	public void setLogic(ConfigViewerLogic logic) {
		this.logic = logic;
	}
	
	private ServerConfigurationService serverConfigurationService;

	/**
	 * Function to allow Spring to inject the server configuration service.
	 * @param logic
	 */
	public void setServerConfigurationService(
			ServerConfigurationService serverConfigurationService) {
		this.serverConfigurationService = serverConfigurationService;
	}
	
//	private ConfigViewerItemBean configViewerItemBean;
//	
//	/**
//	 * Function to allow Spring to inject the tool bean.
//	 * @param logic
//	 */
//	public void setConfigViewerItemBean(ConfigViewerItemBean configViewerItemBean) {
//		this.configViewerItemBean = configViewerItemBean;
//	}
	
  /**
   * Function to allow Spring to inject the Locale. 
   * @param Locale
   */
  private Locale locale;
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

/**
 * Fill the RSF components to make the display for the main Config Viewer page
 * @param tofill The UIContainer to fill
 * @param viewparams The current ViewParameters
 * @param checker The current ComponentChecker
 */	
	public void fillComponents(UIContainer tofill, ViewParameters viewparams,
			ComponentChecker checker) {		

		// we have to bypass RSF to load the help text so that we can check its length
		ResourceLoader helpResourceLoader = new ResourceLoader("org.sakaiproject.configviewer.propertiesHelpMessages");
		ResourceLoader messagesResourceLoader = new ResourceLoader("org.sakaiproject.configviewer.tool.bundle.messages");

		// make absolutely sure that only admins can view this information
		if (! this.logic.isAdmin()) {
			//throw new SecurityException(messageLocator.getMessage("cv.admin.notice")); 
			// this should apparently use a Bean Guard, which I'm sure Antranig will try to explain to me unsuccessfully and end up writing by proxy.
			throw new SecurityException("You must be an admin to use this tool."); 
		}
		
		// use a date which is related to the current users locale
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, locale);
		
		String[] pageLengths = new String[] {"10","25","50"};
		ConfigViewerParameters cvp = (ConfigViewerParameters) viewparams;
		
		// The text inside the page's <title> tags
		UIMessage.make(tofill,"config-viewer-properties-page-title","cv.properties.pagetitle");  
		
		// The links between sections of the tool
		UIMessage.make(tofill, "config-viewer-properties-link", "cv.properties.link");
		UIInternalLink.make(tofill, "config-viewer-text-link", UIMessage.make("cv.text.link"), new PropertyTextParameters(PropertyTextProducer.VIEW_ID)); 
		
		// The title of the properties viewer tool
		//UIOutput.make(tofill,"config-viewer-properties-title",messageLocator.getMessage("cv.properties.title"));  
		UIMessage.make(tofill,"config-viewer-properties-title","cv.properties.title");  

		// The text for the "records per page" picker
		UIMessage.make(tofill,"config-viewer-display-text", "cv.recordsperpage.leader");  
		UIMessage.make(tofill,"config-viewer-records-per-page-text", "cv.recordsperpage.trailer"); 
		
		// The headers for the data table
		UIMessage.make(tofill, "config-viewer-name-header", "cv.header.name");  
		UIMessage.make(tofill, "config-viewer-value-header", "cv.header.value");  
		UIMessage.make(tofill, "config-viewer-help-header", "cv.header.help");  
		UIMessage.make(tofill, "config-viewer-syntax-header", "cv.header.syntax");  
		UIMessage.make(tofill, "config-viewer-tools-header", "cv.header.tools");  
		UIMessage.make(tofill, "config-viewer-version-header", "cv.header.version");  

		// the text that appears in front of the current date
		UIMessage.make(tofill, "config-viewer-generated-on-text", "cv.generatedon");  
		UIOutput.make(tofill, "current-date", df.format(new Date()));

		List l;
		if (cvp.filterText != null && cvp.filterText.length() > 0) {
			ToolMessageResolver messageResolver = new ToolMessageResolver();
			l = logic.getFilteredItems(cvp.filterText, messageResolver);
		}
		else {	l = logic.getAllItems(); }


		// make the search form
		UIBranchContainer searchBranch = UIBranchContainer.make(tofill, "config-viewer-filtertext-branch:"); 
		UIForm searchForm = UIForm.make(searchBranch, "config-viewer-filtertext-form", viewparams);		 

		// add a hidden variable to reset the navigation
		UIInput.make(searchForm, "startRecord", null , "1");  

		// add the filter/search input field
		UIInput.make(searchForm, "filterText", null , cvp.filterText); 
		UIMessage.make(searchForm, "filterTextSubmit", "cv.filtertext.submit");
		
		// add a link to clear the search
		ConfigViewerParameters clearFiltertextParams = new ConfigViewerParameters(ConfigViewerProducer.VIEW_ID, 1, cvp.recordsPerPage, ""); 
		UIInternalLink.make(searchForm, "config-viewer-filtertext-clear", UIMessage.make("cv.filtertext.clearlink"), clearFiltertextParams);  
		
		// make the "records per page" form
		UIBranchContainer recordsPerPageBranch = UIBranchContainer.make(tofill, "config-viewer-recordsperpage-branch:"); 
		UIForm recordsPerPageForm = UIForm.make(recordsPerPageBranch, "config-viewer-recordsperpage-form", viewparams);		 

		UIMessage.make(recordsPerPageForm, "recordsPerPageSubmit", "cv.recordsperpage.submit");
		
		// add the "records per page" picker
		UISelect.make(recordsPerPageForm, "recordsPerPage", pageLengths, "#{recordsPerPage}", String.valueOf(cvp.recordsPerPage));

		// now make the navigational links
		if (l.size() > cvp.recordsPerPage) {
			for (int j=0; j < l.size(); j += cvp.recordsPerPage) {
				int endRecord=0;
				if ((j + cvp.recordsPerPage) <= l.size()) {
					endRecord = (j + cvp.recordsPerPage);					
				}
				else {
					endRecord = l.size();
				}
				UIBranchContainer navEntry = UIBranchContainer.make(tofill, "config-viewer-nav-entry:", Integer.toString(j)); 

				ConfigViewerParameters myParams = new ConfigViewerParameters(ConfigViewerProducer.VIEW_ID, j + 1, cvp.recordsPerPage, cvp.filterText);

				UIInternalLink.make(navEntry, "config-viewer-nav-link", (j + 1) + "-" + endRecord, myParams);  
			}					
		}		
				
		int currentEndRecord = 0;
		if ((cvp.startRecord + cvp.recordsPerPage) <= l.size()) {
			currentEndRecord = (cvp.startRecord + cvp.recordsPerPage - 1);					
		}
		else {
			currentEndRecord = l.size();
		}

		// we need to build this after the nav bar so that we get a reasonable value for endRecord
		//UIOutput.make(tofill,"config-viewer-status", messageLocator.getMessage("cv.status.leader") + cvp.startRecord + messageLocator.getMessage("cv.status.joiner") + currentEndRecord + messageLocator.getMessage("cv.status.joiner2") + l.size() );
		UIMessage.make(tofill,"config-viewer-status","cv.status", new Object[]{ Integer.toString(cvp.startRecord), Integer.toString(currentEndRecord), Integer.toString(l.size()) } );
		
		// now fill in the actual data
		for (int row=0; row < l.size(); row ++) {
			ConfigViewerItem item = (ConfigViewerItem) l.get(row);
			
			PropertyDetailParameters pdp = new PropertyDetailParameters(PropertyDetailProducer.VIEW_ID, item.getId());
			
			if (row >= (cvp.startRecord-1) && row < ((cvp.startRecord-1) + cvp.recordsPerPage)) {
	
				UIBranchContainer configEntry = UIBranchContainer.make(tofill, "config-entry:", item.getId() ); 
	
				if ((row % 2) != 0) {
					Map<String, String> attrmap = new HashMap<String, String>();
					attrmap.put("class", "configEntryOddRow");  
					configEntry.decorators = new DecoratorList(new UIFreeAttributeDecorator(attrmap)); 
				}
				
				UIOutput.make(configEntry, "config-entry-name", item.getId()); 

				if (item.getType().equals("getStrings")) {
					String[] currentValueList = serverConfigurationService.getStrings(item.getId());
					if (currentValueList == null) {
						UIBranchContainer configEntryValueBranch = UIBranchContainer.make(configEntry,"config-entry-value-branch:", "cevb_"+row+"_0"); 
						UIMessage configEntryValue = UIMessage.make(configEntryValueBranch, "config-entry-value", "cv.unset");											 
						Map<String, String> greyTextAttrmap = new HashMap<String, String>();
						greyTextAttrmap.put("style", "font-style:italic;color:#999999;");  
						configEntryValue.decorators = new DecoratorList(new UIFreeAttributeDecorator(greyTextAttrmap)); 

					}
					else {
						for (int d=0; d< currentValueList.length; d++) {
							UIBranchContainer configEntryValueBranch = UIBranchContainer.make(configEntry,"config-entry-value-branch:", "cevb_"+row+"_"+d); 

							// don't attempt to break up HTML into shorter blocks, and display it verbatim
							if ("html".equals(item.getRule())) {
								UIVerbatim.make(configEntryValueBranch, "config-entry-value", currentValueList[d]);
							}
							// if the property value is too large, truncate the display and generate a "more" link with the full text
							else if (currentValueList[d].length() > MAX_WIDTH) {
								UIOutput.make(configEntryValueBranch, "config-entry-value", currentValueList[d].substring(0,MAX_WIDTH - 3) + "...");											 

								UIBranchContainer tooltipBranch = UIBranchContainer.make(tofill, "expanded-text-branch:", "cevet_"+row+"_"+d);
								UIOutput configEntryValueExpandedText = UIOutput.make(tooltipBranch, "expanded-text", currentValueList[d]);
								//UILink configEntryValueLink = UIInternalLink.make(configEntryValueBranch, "config-entry-value-link", messageLocator.getMessage("cv.morelink"),  pdp);
								UILink configEntryValueLink = UIInternalLink.make(configEntryValueBranch, "config-entry-value-link", UIMessage.make("cv.morelink"),  pdp);

								if (logic.useJavascript()) {
									// build the attribute map for the tooltip links
									Map<String, String> tooltipAttrmap = new HashMap<String, String>();
									tooltipAttrmap.put("onMouseOver", "showTooltip('"+ configEntryValueExpandedText.getFullID() +"', this);");  
									tooltipAttrmap.put("onMouseOut", "hideTooltip('"+ configEntryValueExpandedText.getFullID() +"');");  
									tooltipAttrmap.put("onClick", "detailPopup(this.href);return false;");  
									configEntryValueLink.decorators = new DecoratorList(new UIFreeAttributeDecorator(tooltipAttrmap)); 								
								}
							}
							else {
								UIOutput.make(configEntryValueBranch, "config-entry-value", currentValueList[d]);											 
							}
						}
					}
				}
				else {
					String currentValue = serverConfigurationService.getString(item.getId(),"");

					// only need one branch container for strings 
					UIBranchContainer configEntryValueBranch = UIBranchContainer.make(configEntry,"config-entry-value-branch:", "cevb_"+row); 

					// only display values that are set
					if (currentValue.length() > 0) {
						// display html values verbatim
						if ("html".equals(item.getRule())) {
							UIVerbatim.make(configEntryValueBranch, "config-entry-value", currentValue);
						}
						// if the property value is too large, truncate the display and generate a "more" link with the full text
						else if (currentValue.length() > MAX_WIDTH) {
							UIOutput.make(configEntryValueBranch, "config-entry-value", currentValue.substring(0,MAX_WIDTH - 3) + "...");				 

							UIBranchContainer tooltipBranch = UIBranchContainer.make(tofill, "expanded-text-branch:", "cevet_"+row);
							UIOutput configEntryValueExpandedText = UIOutput.make(tooltipBranch, "expanded-text", currentValue);
							UILink configEntryValueLink = UIInternalLink.make(configEntryValueBranch, "config-entry-value-link", UIMessage.make("cv.morelink"),  pdp);
							
							if (logic.useJavascript()) {
								// build the attribute map for the tooltip links
								Map<String, String> tooltipAttrmap = new HashMap<String, String>();
								tooltipAttrmap.put("onMouseOver", "showTooltip('"+ configEntryValueExpandedText.getFullID() +"', this);");  
								tooltipAttrmap.put("onMouseOut", "hideTooltip('"+ configEntryValueExpandedText.getFullID() +"');");  
								tooltipAttrmap.put("onClick", "detailPopup(this.href);return false;");  
								configEntryValueLink.decorators = new DecoratorList(new UIFreeAttributeDecorator(tooltipAttrmap)); 								
							}
						}
						else {
							UIOutput.make(configEntryValueBranch, "config-entry-value", currentValue);				 
						}
					}
					else {
						UIMessage configEntryValue = UIMessage.make(configEntryValueBranch, "config-entry-value", "cv.unset");	  
						Map<String, String> greyTextAttrmap = new HashMap<String, String>();
						greyTextAttrmap.put("style", "font-style:italic;color:#999999;");  
						configEntryValue.decorators = new DecoratorList(new UIFreeAttributeDecorator(greyTextAttrmap)); 
					}
				}
								
				// if the help text is too large, truncate the display and generate a "more" link with the full text
				UIBranchContainer configEntryHelpBranch =  UIBranchContainer.make(configEntry, "config-entry-help-branch:", "cehb_"+row); 
				String itemHelpText = helpResourceLoader.getString(item.getId());

				if (itemHelpText.length() > MAX_WIDTH) {
					UIOutput.make(configEntryHelpBranch,"config-entry-help", itemHelpText.substring(0,MAX_WIDTH - 3) + "...");

					UIBranchContainer tooltipBranch = UIBranchContainer.make(tofill, "expanded-text-branch:", "cehet_"+row);
					UIOutput configEntryHelpExpandedText = UIOutput.make(tooltipBranch, "expanded-text", itemHelpText);
					UILink configEntryHelpLink = UIInternalLink.make(configEntryHelpBranch, "config-entry-help-link", UIMessage.make("cv.morelink"),  pdp);

					if (logic.useJavascript()) {
						// build the attribute map for the tooltip links
						Map<String, String> tooltipAttrmap = new HashMap<String, String>();
						tooltipAttrmap.put("onMouseOver", "showTooltip('"+ configEntryHelpExpandedText.getFullID() +"', this);");  
						tooltipAttrmap.put("onMouseOut", "hideTooltip('"+ configEntryHelpExpandedText.getFullID() +"');");  
						tooltipAttrmap.put("onClick", "detailPopup(this.href);return false;");  
						configEntryHelpLink.decorators = new DecoratorList(new UIFreeAttributeDecorator(tooltipAttrmap)); 														
					}
				}
				else {
				UIMessage.make(configEntryHelpBranch, "config-entry-help", item.getId()); 
				}

				String itemSyntaxText = "";
				if ("getStrings".equals(item.getType())) {
					itemSyntaxText = messagesResourceLoader.getFormattedMessage("syntax_getstrings", new Object[] {item.getId(), item.getQualifier()});
				}
				else {
					itemSyntaxText = messagesResourceLoader.getFormattedMessage("syntax_"+item.getRule(), new Object[] {item.getQualifier()});
				}
				
				// if the syntax description is too large, truncate the display and generate a "more" link with the full text
				UIBranchContainer configEntrySyntaxBranch =  UIBranchContainer.make(configEntry, "config-entry-syntax-branch:","cesb_"+row); 
				if (itemSyntaxText.length() > MAX_WIDTH) {
					UIOutput.make(configEntrySyntaxBranch,"config-entry-syntax", itemSyntaxText.substring(0,MAX_WIDTH - 3) + "...");

					UIBranchContainer tooltipBranch = UIBranchContainer.make(tofill, "expanded-text-branch:", "ceset_"+row);
					UIOutput configEntrySyntaxExpandedText = UIOutput.make(tooltipBranch, "expanded-text", itemSyntaxText);
					UILink configEntrySyntaxLink = UIInternalLink.make(configEntrySyntaxBranch, "config-entry-syntax-link", UIMessage.make("cv.morelink"),  pdp);
					
					if (logic.useJavascript()) {
						// build the attribute map for the tooltip links
						Map<String, String> tooltipAttrmap = new HashMap<String, String>();
						tooltipAttrmap.put("onMouseOver", "showTooltip('"+ configEntrySyntaxExpandedText.getFullID() +"', this);");  
						tooltipAttrmap.put("onMouseOut", "hideTooltip('"+ configEntrySyntaxExpandedText.getFullID() +"');");  
						tooltipAttrmap.put("onClick", "detailPopup(this.href);return false;");  
						configEntrySyntaxLink.decorators = new DecoratorList(new UIFreeAttributeDecorator(tooltipAttrmap)); 								
					}
				}
				else {
					UIMessage.make(configEntrySyntaxBranch, "config-entry-syntax", "syntax_"+item.getRule(), new Object[] {item.getQualifier()}); 
				}

				// if the list of tools is too large, truncate the display and generate a "more" link with the full text
				UIBranchContainer configEntryToolBranch =  UIBranchContainer.make(configEntry, "config-entry-tool-branch:", "cetb_"+row); 
				if (item.getTool().length() > MAX_WIDTH) {
					UIOutput.make(configEntryToolBranch,"config-entry-tool", item.getTool().substring(0,MAX_WIDTH - 3) + "...");

					UIBranchContainer tooltipBranch = UIBranchContainer.make(tofill, "expanded-text-branch:", "cetet_"+row);
					UIOutput configEntryToolExpandedText = UIOutput.make(tooltipBranch, "expanded-text", item.getTool());
					//UILink configEntryToolLink = UIInternalLink.make(configEntryToolBranch, "config-entry-tool-link", messageLocator.getMessage("cv.morelink"),  pdp);
					UILink configEntryToolLink = UIInternalLink.make(configEntryToolBranch, "config-entry-tool-link", UIMessage.make("cv.morelink"),  pdp);

					if (logic.useJavascript()) {
						// build the attribute map for the tooltip links
						Map<String, String> tooltipAttrmap = new HashMap<String, String>();
						tooltipAttrmap.put("onMouseOver", "showTooltip('"+ configEntryToolExpandedText.getFullID() +"', this);");  
						tooltipAttrmap.put("onMouseOut", "hideTooltip('"+ configEntryToolExpandedText.getFullID() +"');");  
						configEntryToolLink.decorators = new DecoratorList(new UIFreeAttributeDecorator(tooltipAttrmap)); 								
						tooltipAttrmap.put("onClick", "detailPopup(this.href);return false;");  
					}
				}
				else {
					UIOutput.make(configEntryToolBranch, "config-entry-tool", item.getTool()); 
				}

				UIOutput.make(configEntry, "config-entry-version", item.getVersion()); 
			}
		}
	}

	public ViewParameters getViewParameters() {
		return new ConfigViewerParameters(VIEW_ID,1,25);
	}


}
