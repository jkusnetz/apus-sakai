/******************************************************************************
 * tooltip.js - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 Virginia Polytechnic Institute and State University
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/


function showTooltip(id,callingObject) {
  //alert('id:' + id);
  divObject = document.getElementById(id);
  if (divObject) {
	 // we have to unhide the div or we can't get the height and width
     divObject.style.display="";

     var leftPos = getRealLeft(callingObject);
     var rightPos = leftPos + callingObject.offsetWidth + divObject.offsetWidth;

     var topPos = getRealTop(callingObject);
     var bottomPos = topPos + divObject.offsetHeight;

     //alert("leftPos=" + leftPos + ", rightPos=" + rightPos + ", topPos=" + topPos + ", bottomPos=" + bottomPos + ",window.innerWidth=" + window.innerWidth);

	 // don't position the div under the pointer or the mouseOut handler may fire
     if (rightPos < window.innerWidth) {
		divObject.style.left=(leftPos + callingObject.offsetWidth + 5) + "px";
     }
     else {
        divObject.style.left=(leftPos - divObject.offsetWidth - 5) + "px";
     }


     if (bottomPos < window.innerHeight) {
        divObject.style.top=topPos + 5 + "px";
     }
     else {
        divObject.style.top=topPos - (divObject.offsetHeight - callingObject.offsetHeight) + "px";
     }


     // reveal the tooltip
     divObject.style.display="";
  }
  else {
     alert("Element with id " + id + " not found...");
  }
}

function hideTooltip(id) {
     divObject = document.getElementById(id);
     if (divObject) {
         divObject.style.display="none";
     }
}

function getRealLeft(object) {
     var realLeft = object.offsetLeft;

     var workingObject = object;
     while (workingObject.offsetParent!=null) {
        realLeft += workingObject.offsetParent.offsetLeft;
	workingObject = workingObject.offsetParent;
     }

     return realLeft;
}

function getRealTop(object) {
     var realTop = object.offsetTop;

     var workingObject = object;
     while (workingObject.offsetParent!=null) {
        realTop += workingObject.offsetParent.offsetTop;
	workingObject = workingObject.offsetParent;
     }

     return realTop;
}