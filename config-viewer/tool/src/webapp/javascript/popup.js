/******************************************************************************
 * popup.js - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 UHI Millenium Institute
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/
 
 function detailPopup(popupLocation) {
 	window.open(popupLocation,'detailPopup','toolbar=0,location=0,menubar=0,resizable=1,scrollable=1,width=600,height=400');
 }