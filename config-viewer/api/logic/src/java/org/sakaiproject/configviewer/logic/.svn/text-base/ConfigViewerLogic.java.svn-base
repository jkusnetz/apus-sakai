package org.sakaiproject.configviewer.logic;

import java.util.List;

import org.sakaiproject.configviewer.model.ConfigViewerItem;

/**
 * @author Anthony Atkins (duhrer@gmail.com)
 *
 */
public interface ConfigViewerLogic {
	/**
	 * Return the complete list of supported configuration properties.
	 * @return a List of ConfigViewerItem objects
	 */
	public List getAllItems();

	/**
	 * Return the filtered list of supported configuration properties.
	 * @param filterText a regular expression to filter the overall list of results by
	 * @return a List of ConfigViewerItem objects
	 */
	public List getFilteredItems(String filterText, MessageResolver messageResolver);

	/**
	 * Return a single configuration property.
	 * @param propertyKey a string specifying the key for a single configuration property
	 * @return a ConfigViewerItem or null if no property is found.
	 */
	public ConfigViewerItem getItem(String propertyKey);
	
	/**
	 * A wrapper to check for the appropriate permissions. Uses the UserDirectoryService to check the permissions.
	 * @return true if user is an admin, otherwise return false
	 */
	public boolean isAdmin();
	
	/**
	 * Whether or not to use Javascript to hide longer text blocks.
	 * Controlled by the configviewer.usejavascript setting in sakai.properties
	 * @return true if we should use javascript, false otherwise.
	 */
	public boolean useJavascript();
}
