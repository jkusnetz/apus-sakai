package org.sakaiproject.configviewer.logic;

public interface MessageResolver {
	public String resolveMessage(String key);
}
