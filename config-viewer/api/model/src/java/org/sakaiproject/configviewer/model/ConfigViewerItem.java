/******************************************************************************
 * ConfigViewerItem.java - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 Virginia Polytechnic Institute and State University
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/

package org.sakaiproject.configviewer.model;

/**
 * @author Anthony Atkins (duhrer@gmail.com)
 *
 */
public class ConfigViewerItem {
	private String id;
	private String type;
	private String tool;
	private String version;
	private String rule;
	private String qualifier;
	
	/**
	 * Build a ConfigViewerItem from a complete list of options.
	 * @param id The unique identifier (example: auto.ddl) for the configuration property.
	 * @param type The type of configuration property (string, boolean, integer, nested string, bean property).
	 * @param currentValue The current value of the property, as determined by polling the @see org.sakaiproject.component.cover.ServerConfigurationService.
	 * @param helpText The help text for the property, a description of its purpose.
	 * @param version The versions of Sakai for which the property is used.
	 * @param rule The rule to match when validating this property.
	 * @param qualifier Additional information about the rule to use when validating this property.
	 */
	public ConfigViewerItem(String id, String type, String tool, String version, String rule, String qualifier) {
		this.id = id;
		this.type = type;
		this.tool = tool;
		this.version = version;
		this.rule = rule;
		this.qualifier = qualifier;
	}

	/**
	 * Build a skeletal ConfigViewerItem from an id and type.
	 * @param id The unique identifier (example: auto.ddl) for the configuration property.
	 * @param type The type of configuration property (string, boolean, integer, nested string, bean property).
	 */
	public ConfigViewerItem(String id, String type) {
		this.id = id;
		this.type = type;
	}

	/**
	 * Return the unique identifier for the configuration property.
	 * @return id The unique identification string (example: auto.ddl) for the property.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Set the unique identifier for the configuration property.
	 * @param id The unique identification string (example: auto.ddl) for the property.
	 */
	public void setId(String id) {
		this.id = id;
	}

  /**
   * Return the list of tools that use the configuration property.
   * @return tool The String value describing the list of tools that use this configuration property.
   */
	public String getTool() {
		return tool;
	}

  /**
   * Set the list of tools that use the configuration property.
   * @param tool The String value describing the list of tools that use this configuration property.
   */
	public void setTool(String tool) {
		this.tool = tool;
	}

  /**
   * Return the type of configuration property.
   * @return type The String value describing the type of this configuration property (string, boolean, int, array of strings).
   */
	public String getType() {
		return type;
	}

  /**
   * Set the type of configuration property.
   * @param type The String value describing the type of this configuration property (string, boolean, int, array of strings).
   */
	public void setType(String type) {
		this.type = type;
	}

  /**
   * Return the versions of Sakai that use this configuration property.
   * @return version The String value describing the versions of Sakai that use this configuration property.
   */
	public String getVersion() {
		return version;
	}

  /**
   * Set the versions of Sakai that use this configuration property.
   * @param version The String value describing the versions of Sakai that use this configuration property.
   */
	public void setVersion(String version) {
		this.version = version;
	}

	
	/**
	 * @return the rule
	 */
	public String getRule() {
		return rule;
	}
	
	/**
	 * @param rule the rule to set
	 */
	public void setRule(String rule) {
		this.rule = rule;
	}
	
	/**
	 * @return the qualifier
	 */
	public String getQualifier() {
		return qualifier;
	}
	
	/**
	 * @param qualifier the qualifier to set
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	/**
	 * Get a concatenated string containing all values for this item
	 * @return allValueString a string containing a concatenated list of all values
	 */
	public String getAllValues() {		
		String allValueString = 
			"id:" + id +
			";type:" + type +
			";tool:" + tool +
			";version:" + version +
			";rule:" + rule +
			";qualifier:" + qualifier;
		return allValueString;
	}
}
