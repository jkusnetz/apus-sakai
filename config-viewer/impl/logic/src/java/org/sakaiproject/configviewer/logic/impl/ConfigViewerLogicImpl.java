/******************************************************************************
 * ConfigViewerLogicImpl.java - created by duhrer@gmail.com
 * 
 * Copyright (c) 2007 Virginia Polytechnic Institute and State University
 * Licensed under the Educational Community License version 1.0
 * 
 * A copy of the Educational Community License has been included in this 
 * distribution and is available at: http://www.opensource.org/licenses/ecl1.php
 * 
 * Contributors:
 * Anthony Atkins (duhrer@gmail.com)
 * 
 *****************************************************************************/

package org.sakaiproject.configviewer.logic.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sakaiproject.authz.api.SecurityService;
import org.sakaiproject.component.cover.ServerConfigurationService;
import org.sakaiproject.configviewer.logic.ConfigViewerLogic;
import org.sakaiproject.configviewer.logic.MessageResolver;
import org.sakaiproject.configviewer.model.ConfigViewerItem;
import org.sakaiproject.tool.api.SessionManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;


public class ConfigViewerLogicImpl implements ConfigViewerLogic, ApplicationContextAware {
	private static Log log = LogFactory.getLog(ConfigViewerLogicImpl.class);

	private List<ConfigViewerItem> configViewerItems = new ArrayList<ConfigViewerItem>();
  
	private SecurityService securityService;
	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}
	
	private ApplicationContext applicationContext;	
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	private SessionManager sessionManager;
	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}
	
	public boolean isAdmin() {
		try {
			String currentUserId;
			currentUserId = this.sessionManager.getCurrentSessionUserId();

			if (securityService.isSuperUser(currentUserId)) {
				return true;
			}			
		} catch (Exception e) {
			log.error("Can't check admin status...", e);
		}

		return false;
	}
	/**
	 * Place any code that should run when this class is initialized by spring here
	 * @throws Exception 
	 */
	public void init() {
		log.debug("initializing ConfigViewer property viewer");
		
		if (ServerConfigurationService.getString("configviewer.usejavascript").equalsIgnoreCase("true")) {
			log.debug("Config Viewer javascript tooltips are turned on...");
		}
		else {
			log.debug("Config Viewer javascript tooltips are turned off...");
		}

		// We need to load from sakai.properties because we can't read a lot of bean properties directly

		// load the bundled version of sakai.properties from the classpath
        // This location changed in 2.6
		Resource sakaiBundledPropertiesResource = applicationContext.getResource("classpath:org/sakaiproject/config/bundle/default.sakai.properties");
		Properties sakaiBundledProperties = new Properties();
		
		HashMap<String,String> sakaiBundledPropertiesValues = new HashMap<String,String>();
		
		try { 
			sakaiBundledProperties.load(sakaiBundledPropertiesResource.getInputStream());
			for (Enumeration sakaiBundledPropertiesEnumerator = sakaiBundledProperties.keys(); sakaiBundledPropertiesEnumerator.hasMoreElements();) {
				String sakaiBundledPropertiesKey = (String) sakaiBundledPropertiesEnumerator.nextElement();
				String sakaiBundledPropertiesValue = sakaiBundledProperties.getProperty(sakaiBundledPropertiesKey);
				sakaiBundledPropertiesValues.put(sakaiBundledPropertiesKey, sakaiBundledPropertiesValue );
			}
		}
		catch (Exception e) {
			log.error("Can't load bundled sakai.properties file (for Config Viewer)...");
			e.printStackTrace();
		}

		// Now load the user-supplied sakai.properties file from the file system
		Properties sakaiFileProperties = new Properties();
		
		HashMap<String,String> sakaiFilePropertiesValues = new HashMap<String,String>();
		
		try { 
			File sakaiPropertiesFile = new File(System.getProperty("sakai.home") + File.separatorChar + "sakai.properties");
			FileInputStream sakaiPropertiesFileInputStream = new FileInputStream(sakaiPropertiesFile);
			sakaiFileProperties.load(sakaiPropertiesFileInputStream);
			
			for (Enumeration<Object> sakaiFilePropertiesEnumerator = sakaiFileProperties.keys(); sakaiFilePropertiesEnumerator.hasMoreElements();) {
				String sakaiFilePropertiesKey = (String) sakaiFilePropertiesEnumerator.nextElement();
				String sakaiFilePropertiesValue = sakaiFileProperties.getProperty(sakaiFilePropertiesKey);
				sakaiFilePropertiesValues.put(sakaiFilePropertiesKey, sakaiFilePropertiesValue );
			}
			sakaiPropertiesFileInputStream.close();
		}
		catch (Exception e) {
			log.error("Can't load user-supplied sakai.properties file (for Config Viewer)...");
			e.printStackTrace();
		}
		
		Properties configViewerProperties = new Properties();
		
		Resource configViewerPropertiesResource = applicationContext.getResource("classpath:org/sakaiproject/configviewer/configViewer.properties");
		if (configViewerPropertiesResource.exists()) {
			log.debug("found configViewer.properties file!");

			try {
				InputStream in = configViewerPropertiesResource.getInputStream();

				try { 
					configViewerProperties.load(in); 
	
					/* now try to work with the count of properties */
					if (configViewerProperties.getProperty("properties.count") != null) {
	
						int propertyCount;
						try {
							propertyCount = new Integer(configViewerProperties.getProperty("properties.count")).intValue();
						} catch (NumberFormatException e) {
							log.error("Error reading properties file; properties.count must be a number.");
							return;
						}
	
						for (int a=1; a <= propertyCount; a++) {
							log.debug("reading Config Viewer property " + a);
							if (configViewerProperties.getProperty("properties.name."+a) != null &&
									configViewerProperties.getProperty("properties.type."+a) != null) {
								String name = configViewerProperties.getProperty("properties.name."+a);
								String type = configViewerProperties.getProperty("properties.type."+a);
								String help = configViewerProperties.getProperty("properties.help."+a);
								String tool = configViewerProperties.getProperty("properties.tool."+a);
								String version = configViewerProperties.getProperty("properties.version."+a);
								String rule = configViewerProperties.getProperty("properties.rule."+a);
								String qualifier = configViewerProperties.getProperty("properties.qualifier."+a);
								
								/* Strings, Ints, and Booleans can all be read as strings for the purposes of this app */
								configViewerItems.add(new ConfigViewerItem(name,type,tool,version,rule,qualifier));
							}	
							else {
								log.warn("Can't find enough information to construct a record for property " + a );
							}
						}
					}
					else {
						log.error("You must set at least the property properties.count");
					}
				}
				catch (java.io.IOException e) {
					log.error("Can't load config viewer properties file...");
					e.printStackTrace();
				}
			}
			catch (java.io.IOException e){
				log.error("Can't open config viewer properties input stream...");
			}
		}
		else {
			log.error("Config viewer properties resource does not exist...");
		}
	}

	/* (non-Javadoc)
	 * @see org.sakaiproject.configviewer.logic.ConfigViewerLogic#getAllItems(java.lang.String)
	 */
	public List getAllItems() {
		log.debug("Fetching all items");

		return configViewerItems;
	}

	public List getFilteredItems(String filterText, MessageResolver messageResolver) {
		log.debug("Fetching a filtered list of items");
		
		List<ConfigViewerItem> filteredConfigViewerItems = new ArrayList<ConfigViewerItem>();

		// I would love to make a simple query parser that tokenises the filterText
		// The query parser would build a list of qualified required, optional, and negated search terms
		ArrayList<String> requiredTerms = new ArrayList<String>();
		ArrayList<String> negatedTerms = new ArrayList<String>();

		String[] filterTextTokens = filterText.split(" ");
		
		boolean quoteOpen = false;
		boolean required = false;
		boolean forbidden = false;
		
		String filterTextToken = ""; 
				
		for (int b = 0; b < filterTextTokens.length; b++) {
//			log.info("token parsing pass " + b + ":" + filterTextTokens[b]);
			if (quoteOpen) {
				if (filterTextTokens[b].endsWith("\"")) {
					log.info("closing quote with phrase:" + filterTextTokens[b]);
					filterTextToken += " " + filterTextTokens[b].substring(0, filterTextTokens[b].length() - 1);
					quoteOpen = false;

					if (forbidden) { negatedTerms.add(filterTextToken); }
					else if (required && !requiredTerms.contains(filterTextToken) && !negatedTerms.contains(filterTextToken)) { requiredTerms.add(filterTextToken); }
					else { log.info("Can't determine whether phrase " + filterTextToken + " is required or negated."); }

				}
				else if (b == (filterTextTokens.length - 1)) {
					log.info("finishing string with phrase:" + filterTextTokens[b]);
					filterTextToken += " " + filterTextTokens[b];
					quoteOpen = false;
				}				
				else {
					log.info("continuing string with phrase: " + filterTextTokens[b]);
					filterTextToken += " " + filterTextTokens[b];
				}
			}
			else {
//				log.info("not part of a quoted block");
				if (filterTextTokens[b].startsWith("-\"")) {
					required  = false;
					forbidden = true;
					quoteOpen = true;
					filterTextToken=filterTextTokens[b].substring(2);
				}
				else if (filterTextTokens[b].startsWith("-")) {
					required  = false;
					forbidden = true;
					filterTextToken = filterTextTokens[b].substring(1);
				}
				else if (filterTextTokens[b].startsWith("+\"")) {
					log.info("stripping plus sign from beginning of phrase:" + filterTextTokens[b]);
					required  = true;
					forbidden = false;
					quoteOpen = true;
					filterTextToken = filterTextTokens[b].substring(2);
				}
				else if (filterTextTokens[b].startsWith("+")) {
					log.info("stripping plus sign from word:" + filterTextTokens[b]);
					required = true;
					forbidden = false;
					filterTextToken = filterTextTokens[b].substring(1);
				}
				else if (filterTextTokens[b].startsWith("\"")) {
					log.info("opening quote with phrase:" + filterTextTokens[b]);
					filterTextToken = filterTextTokens[b].substring(1);
					quoteOpen = true;
					required = true;
				}
				else {
					required = true;
					forbidden = false;
					filterTextToken = filterTextTokens[b];
				}
			}
			
			// If we're not in the middle of a quoted string, go ahead and sort this token
			if (!quoteOpen ) {
				if (forbidden) {
//					log.info("negatedTerm: " + filterTextToken);
					negatedTerms.add(filterTextToken);
				}
				else if (required && !requiredTerms.contains(filterTextToken) && !negatedTerms.contains(filterTextToken)) {
//					log.info("requiredTerm: " + filterTextToken);
					requiredTerms.add(filterTextToken);					
				}
				else {
					log.info("oops, ate a search term:" + filterTextTokens[b]);
				}
				filterTextToken = "";
			}
		}
		
		if (requiredTerms != null) { log.info("required terms:" + requiredTerms.toString()); }
		if (negatedTerms != null) { log.info("negated terms:" + negatedTerms.toString()); }
		
		for (int a = 0; a < this.configViewerItems.size(); a++) {
			// compare selected attributes to the filterText
			ConfigViewerItem myConfigViewerItem = (ConfigViewerItem) this.configViewerItems.get(a);
			String itemHelpText = messageResolver.resolveMessage(myConfigViewerItem.getId());

			// make sure the item should not be excluded first
			boolean excludeTerm = false;
			for (int b=0; b < negatedTerms.size(); b++) {
				if (negatedTerms.get(b).startsWith("name:")) {
					if (myConfigViewerItem.getId().toLowerCase().contains(negatedTerms.get(b).substring(5).toLowerCase())) {
						log.debug("excluded item with name matching " + negatedTerms.get(b).substring(5));
						excludeTerm = true;
					}
				}
				else if (negatedTerms.get(b).startsWith("help:")) {
					if (itemHelpText.toLowerCase().contains(negatedTerms.get(b).substring(5).toLowerCase())) {
						log.debug("excluded item with help text matching " + negatedTerms.get(b).substring(5));
						excludeTerm = true;
					}
				}
				else if (negatedTerms.get(b).startsWith("tool:")) {
					if (myConfigViewerItem.getTool().toLowerCase().contains(negatedTerms.get(b).substring(5).toLowerCase())) {
						log.debug("excluded item with tool name matching " + negatedTerms.get(b).substring(5));
						excludeTerm = true;
					}
				}
				else if (negatedTerms.get(b).startsWith("version:")) {
					if (myConfigViewerItem.getVersion().toLowerCase().contains(negatedTerms.get(b).substring(8).toLowerCase())) {
						log.debug("excluded item with version matching " + negatedTerms.get(b).substring(8));
						excludeTerm = true;
					}
				}
				else if (myConfigViewerItem.getAllValues().toLowerCase().contains(negatedTerms.get(b).toLowerCase())) {
					log.debug("excluded item matching " + negatedTerms.get(b));
					excludeTerm = true;
				}
			}

			// now check to see if the term should be included
			if (!excludeTerm) {
				boolean includeTerm = true;
				for (int c=0; c < requiredTerms.size(); c++) {
					if (requiredTerms.get(c).startsWith("name:")) {
						if (myConfigViewerItem.getId().toLowerCase().contains(requiredTerms.get(c).substring(5).toLowerCase()) && includeTerm) {
							log.debug("included item with name matching " + requiredTerms.get(c).substring(5));
							includeTerm = true;
						}
						else {
							includeTerm=false;
						}
					}
					else if (requiredTerms.get(c).startsWith("help:")) {
						if (itemHelpText.toLowerCase().contains(requiredTerms.get(c).substring(5).toLowerCase()) && includeTerm) {
							log.debug("included item with help text matching " + requiredTerms.get(c).substring(5));
							includeTerm = true;
						}
						else {
							includeTerm=false;
						}
					}
					else if (requiredTerms.get(c).startsWith("tool:")) {
						if (myConfigViewerItem.getTool().toLowerCase().contains(requiredTerms.get(c).substring(5).toLowerCase()) && includeTerm) {
							log.debug("included item with tool name matching " + requiredTerms.get(c).substring(5));
							includeTerm = true;
						}
						else {
							includeTerm=false;
						}
					}
					else if (requiredTerms.get(c).startsWith("version:") && includeTerm) {
						if (myConfigViewerItem.getVersion().toLowerCase().contains(requiredTerms.get(c).substring(8).toLowerCase())  && includeTerm) {
							log.debug("included item with version matching " + requiredTerms.get(c).substring(8));
							includeTerm = true;
						}
						else { 
							includeTerm=false;
						}
					}
					else if (myConfigViewerItem.getAllValues().toLowerCase().contains(requiredTerms.get(c).toLowerCase()) && includeTerm) {
						includeTerm = true;
					}
					else { includeTerm = false; }
				}					

				if (includeTerm) {
					filteredConfigViewerItems.add(myConfigViewerItem);
				}
			}
		}
		
		return filteredConfigViewerItems;
	}

	public ConfigViewerItem getItem(String propertyKey) {
		log.debug("Fetching a single property");
		
		for (int a = 0; a < this.configViewerItems.size(); a++) {
			// compare selected attributes to the filterText
			ConfigViewerItem myConfigViewerItem = (ConfigViewerItem) this.configViewerItems.get(a);

			String searchString = "^" + propertyKey.toLowerCase() + "$";

				if (myConfigViewerItem.getId().toLowerCase().matches(searchString)) {
						return myConfigViewerItem;
				}
		}
		
		return null;
	}	
	
	public boolean useJavascript() {
		// test to see if javascript tooltips are enabled
		return ServerConfigurationService.getString("configviewer.usejavascript").equalsIgnoreCase("true");
	}	
}
