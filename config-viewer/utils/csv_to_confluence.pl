#!/usr/bin/perl
#
# csv_to_confluence.pl - created by duhrer@gmail.com on February 26, 2008
#
# This script converts a CSV file to a series of Wiki pages.
#
# Copyright (c) 2007, 2008 University of the Highlands and Islands
# Copyright (c) 2006, 2007 Virginia Polytechnic Institute and State University
# 
# A copy of the Educational Community License has been included in this 
# distribution and is available at: http://www.opensource.org/licenses/ecl1.php
#
# Contributors:
# Anthony Atkins (duhrer@gmail.com)

unless ($ARGV[0]) { 
    die("Usage: csv_to_confluence.pl {input_file}\n");
}

$RECORDS_PER_PAGE = 75;
$PAGE_NAME        = "Sakai Properties Reference";

$INDEX_HEADER    = "h1. $PAGE_NAME - Index
h2. Introduction
This document provides links to the list of properties in sections by the first letter of each property name.  To view the full list, [use the full list|$PAGE_NAME - Full].
";
$FULL_HEADER     = "h1. $PAGE_NAME - Full
h2. Introduction
{excerpt}This is a full list of documented configuration options which can be set using the sakai.properties file.  To view this file in sections, [use the alphabetical index|$PAGE_NAME - Index].{excerpt}
----
";

$input_file = $ARGV[0];

open (INPUT, $input_file) or die("Can't read input file $input_file...\n");
$record_number = 0;

my @nameList      = ();
my %cleanNameHash = ();
my %helpHash      = ();
my %toolHash      = ();
my %versionHash   = ();

# read the list of options first
while (<INPUT>) {
    if ($record_number != 0) {
		chomp($_);
	
		# clean up idiosyncrasies with the Excel input
		$_ =~ s/^"//;
		$_ =~ s/"$//;
		$_ =~ s/\t"/\t/g;
		$_ =~ s/"\t/\t/g;
		$_ =~ s/""/"/g;
		$_ =~ s/([\{\}])/\\$1/g;
	
		($name, $type, $help, $tool, $version, $configurable, $rule, $qualifier, $nullable) = split("\t", $_);
		push(@nameList, $name);

                $cleanName = $name;
                $cleanName =~ s/\\?[[\{\}]/_/g;
                $cleanName =~ s/\@/AT/g;
                $cleanNameHash{$name} = $cleanName;

                $help =~ "s/[\r\n]/\\\\/g";

		$helpHash{$name}      = $help;
		$toolHash{$name}      = $tool;
		$versionHash{$name}   = $version;
    }

    $record_number++;
}
close(INPUT);

# output the source documents that will be used to assemble the master documents


# Start by creating one file per property
#
# DISABLED because it generated way too many files 
#
#foreach $name (sort(@nameList)) {
#    open(PROPERTY, ">$PAGE_NAME - " . $cleanNameHash{$name} ) or die("Can't write properties file..."); 
#    print PROPERTY $PROPERTY_HEADER;
#    print PROPERTY "{excerpt}\n";
#    print PROPERTY "{excerpt}\n";
#    close(PROPERTY);
#}


# Create: 
# - An A-Z Index (with links only to the letters with properties)
# - A page for each letter
# - A page with all letters

open (INDEX, ">$PAGE_NAME - Index") or die("Can't write index\n");
print INDEX $INDEX_HEADER;

open (FULL, ">$PAGE_NAME - Full") or die("Can't write full properties list\n");
print FULL $FULL_HEADER;

# lower case
# for ($a=97; $a<123; $a++) {

my $internalNavBar;
my $sectionNavBar;

$found_properties = 0;
for ($a=65; $a<88; $a++) {
    $first_letter = chr($a);
    @propertiesList = grep /^$first_letter/i, @nameList;

    if ($foundProperties > 0) {
	$internalNavBar .=  " | ";
	$sectionNavBar .= " | ";
    }

    if ($#propertiesList > 1) {
	$foundProperties++;

	$internalNavBar .=  "[#$first_letter]";
	$sectionNavBar .= "[$first_letter|$PAGE_NAME - $first_letter]";
    }
    else {
	$internalNavBar .=  "$first_letter";
	$sectionNavBar .= "$first_letter";
    }
}

for ($a=65; $a<88; $a++) {
    $first_letter = chr($a);
    @propertiesList = grep /^$first_letter/i, @nameList;

    if ($#propertiesList > 1) {
        $LETTER_HEADER   = "h2. $PAGE_NAME - $first_letter\n\n||Property Name||Description||\n";

	open (LETTER, ">$PAGE_NAME - $first_letter");
	print LETTER $LETTER_HEADER;
	
	print INDEX "----\n$internalNavBar\n----\n";
        print INDEX "{anchor:$first_letter}\nh2. [$first_letter|$PAGE_NAME - $first_letter]\n";

	foreach $property (sort(@propertiesList)) {
            print LETTER "| {anchor:" . $cleanNameHash{$property} . "} *$property* | ";
            print LETTER $helpHash{$property} . " \\\\ ";
	    print LETTER "_This property is used in Sakai version(s): " . $versionHash{$property} . "_|\n";

            print INDEX "[$property|$PAGE_NAME - $first_letter#" . $cleanNameHash{$property} . "]\n";
	}
	close(LETTER);

	print FULL "{include:$PAGE_NAME - $first_letter}\n";
    }
}

print INDEX "\n";
close (INDEX);

print FULL "\n";
close (FULL);
