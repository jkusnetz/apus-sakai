<?xml version="1.0" encoding="utf-8"?>
<!-- propertyExtractor.xsl - created by duhrer@gmail.com

   Copyright (c) 2007 Virginia Polytechnic Institute and State University
   Licensed under the Educational Community License version 1.0
   
   A copy of the Educational Community License has been included in this 
   distribution and is available at: http://www.opensource.org/licenses/ecl1.php
   
   Contributors:
   Anthony Atkins (duhrer@gmail.com)
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text" version="1.0" standalone="yes" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="/beans/bean"/>
  </xsl:template>

  <xsl:template match="bean">
    <xsl:for-each select="property">
      <xsl:value-of select="@name"/>
      <xsl:text>@</xsl:text>
      <xsl:value-of select="../@id"/>
      <xsl:text>
      </xsl:text>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>