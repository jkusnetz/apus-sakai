#!/bin/sh
#
# configViewer.properties - created by duhrer@gmail.com on November 3, 2006
#
# This script generates a very rough list of server configuration 
# settings that are actually accessed using calls to the 
# Sakai server configuration service.
#
# Copyright (c) 2007 Virginia Polytechnic Institute and State University
# 
# A copy of the Educational Community License has been included in this 
# distribution and is available at: http://www.opensource.org/licenses/ecl1.php
#
# Contributors:
# Anthony Atkins (duhrer@gmail.com)


TIMESTAMP=`date +"%Y-%m-%d_%H%M"`;
OUTPUT_FILE="property_list.$TIMESTAMP";

find . -name \*.java -exec egrep "(ServerConfigurationService)?\.(getBoolean|getString|getStrings|getInt)\(" {} \; | perl -pe "s/.+(ServerConfigurationService)?\.([^\(]+)\(\"([^,\)]+)\".*\).+$/\$1:\$2/" | sort -u > $OUTPUT_FILE

echo "output saved to $OUTPUT_FILE...";