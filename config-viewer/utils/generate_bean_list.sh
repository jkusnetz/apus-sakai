#!/bin/sh
#
# configViewer.properties - created by duhrer@gmail.com on November 3, 2006
#
# This script extracts the full list of beans from the Sakai source.  
# The script requires xsltproc to be available in the path.
#
# Copyright (c) 2006,2007 Virginia Polytechnic Institute and State University
# Copyright (c) 2007 UHI Millenium Institute
# 
# A copy of the Educational Community License has been included in this 
# distribution and is available at: http://www.opensource.org/licenses/ecl1.php
#
# Contributors:
# Anthony Atkins (duhrer@gmail.com)

SAKAI_SOURCE_DIR=/opt/installs/sakai-2.4
XSL_FILE=/opt/installs/sakai-2.4/config-viewer/utils/propertyExtractor.xsl

TIMESTAMP=`date +%Y%m%d_%H%M`;
OUTPUT_FILE=bean_properties_$TIMESTAMP.txt;

if [ -s "$XSL_FILE" ] ; then
    echo "extracting bean properties from source directory $SAKAI_SOURCE_DIR using XSL file $XSL_FILE...";

    find $SAKAI_SOURCE_DIR -name components.xml -exec xsltproc --novalid $XSL_FILE {} >> $OUTPUT_FILE.unsorted \;

    # clean up the file a little
    cat $OUTPUT_FILE.unsorted | perl -pe "s/^ +//" | sort > $OUTPUT_FILE;
    rm $OUTPUT_FILE.unsorted;
    
    echo "Saved bean list to $OUTPUT_FILE...";
else 
    echo "XSL file $XSL_FILE does not exist, can't continue...";
fi