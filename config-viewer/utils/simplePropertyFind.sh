#!/bin/sh
#
# simplePropertyFind.sh: a script to perform a rough search for new properties.
#
# Copyright (c) 2006,2007 Virginia Polytechnic Institute and State University
# Copyright (c) 2007 UHI Millenium Institute
# 
# A copy of the Educational Community License has been included in this 
# distribution and is available at: http://www.opensource.org/licenses/ecl1.php
#
# Contributors:
# Anthony Atkins (duhrer@gmail.com)


if [ -z $1 -o -z $2 ]; then
    echo "Usage: $0 SOURCE_DIR PROPERTIES_FILE";
    exit;
fi

SOURCE_DIR=$1;
if [ ! -d $SOURCE_DIR ]; then
    echo "ERROR: Source directory $SOURCE_DIR does not exist...";
    exit;
fi

PROPERTIES_FILE=$2;
if [ ! -f $PROPERTIES_FILE ]; then
    echo "ERROR: Properties file $PROPERTIES_FILE does not exist...";
    exit;
fi

if [ ! -d $SOURCE_DIR/kernel ]; then
    echo "WARNING: No kernel source found, results are likely to be incomplete...";
fi

if [ -f /tmp/cleaner_results.txt ]; then rm /tmp/cleaner_results.txt; fi

for i in `find $SOURCE_DIR -name \*.java ! -path \*svn\* -exec grep -li serverconfigurationservice\\.get {} \;`; do 
  egrep -B 2 -A 2 -i "ConfigurationService\.get(String|Strings|Boolean|Int)\(" $i | perl -pe "s/[ \r\n\t]+//g" | perl -pe "s/.+ConfigurationService.get(String|Strings|Int|Boolean)\(\"?([^,\)\"]+).+/\$1:\$2/" >> /tmp/cleaner_results.txt; 
  echo >> /tmp/cleaner_results.txt; 
done
sort -u /tmp/cleaner_results.txt > /tmp/unique_results.txt

echo "All detected values saved to /tmp/unique_results.txt ...";

cp /tmp/unique_results.txt /tmp/filtered_results.txt;
for i in `cut -f1 $PROPERTIES_FILE`; do cp /tmp/filtered_results.txt /tmp/filtered_results_xxx; grep -v $i /tmp/filtered_results_xxx > /tmp/filtered_results.txt; done

echo "Values not in current properties file saved to /tmp/filtered_results.txt ...";