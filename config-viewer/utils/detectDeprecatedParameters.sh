#!/bin/sh
#
# detectDeprecatedParameters.sh:  
# A script to detect sakai.properties parameters that are no longer in
# use.  WARNING: This script can take quite a while to run (6-12 hours)
#
# Copyright (c) 2008 UHI Millenium Institute
# 
# A copy of the Educational Community License has been included in this 
# distribution and is available at: http://www.opensource.org/licenses/ecl1.php
#
# Contributors:
# Anthony Atkins (duhrer@gmail.com)
#

if [ -z $1 -o -z $2 ]; then
   echo "Usage: $0 SOURCE_DIR PROPERTIES_FILE ...";
   exit;
fi


SOURCE_DIR=$1;
if [ ! -d $SOURCE_DIR ]; then
   echo "ERROR: Source directory $SOURCE_DIRECTORY does not exist...";
   exit;
fi

PROPERTIES_FILE=$2;
if [ ! -f $PROPERTIES_FILE ]; then
   echo "ERROR: Properties file $PROPERTIES_FILE does not exist...";
   exit;
fi

if [ ! -d $SOURCE_DIR/kernel ]; then
   echo "WARNING: No kernel source found, results may be incomplete...";
fi

if [ -f /tmp/parameterCount.txt ]; then 
    echo -n "Data from previous run detected.  Continue previous run? (y/N)"
    read USE_PREVIOUS_RUN
    if [ $USE_PREVIOUS_RUN = "Y" -o $USE_PREVIOUS_RUN = "y" ]; then
	echo "Continuing previous run..."
    else
	echo "Starting new run..."
	rm /tmp/parameterCount.txt; 
    fi
fi
touch /tmp/parameterCount.txt


FULL_COUNT=`wc -l $PROPERTIES_FILE | perl -pe "s/^[ \t]+//" | cut -d " " -f 1`;
BODY_COUNT=`echo $FULL_COUNT-1|bc`

#for i in `head -2 $PROPERTIES_FILE | tail -1 | cut -f 1`; do 
#for i in `cut -f 1 $PROPERTIES_FILE`; do 
for i in `tail -$BODY_COUNT $PROPERTIES_FILE | cut -f 1`; do 
    echo "Analysing property $i ..."
    if [ "$USE_PREVIOUS_RUN" = "Y" -o "$USE_PREVIOUS_RUN" = "y" ]; then
	COUNT=`cut -d ":" -f 1 /tmp/parameterCount.txt | grep $i | wc -l | perl -pe "s/^[ \t]+//" | cut -d " " -f 1`;
	if [ $COUNT != "0" ]; then
	    echo "$i found in output from previous run.  Skipping...";
	    continue
	fi
    fi

    # check to see if we have hints from the other tool to save time
    if [ -f /tmp/unique_results.txt ]; then
	COUNT=`cut -d ":" -f 2 /tmp/unique_results.txt | grep $i | wc -l | perl -pe "s/^[ \t]+//" | cut -d " " -f 1`;
	if [ $COUNT != "0" ]; then
	    echo "$COUNT instances of $i found in output from simplePropertyFind.sh.  Skipping...";
	    continue
	fi
    fi

    echo "Searching for $i ..."

    if [ -f /tmp/accumulator ]; then rm /tmp/accumulator; fi

    find $SOURCE_DIR ! -path \*svn\* ! -path \*target\* -exec grep -l $i {} \; >> /tmp/accumulator
    COUNT=`wc -l /tmp/accumulator | perl -pe "s/^[ \t]+//" | cut -d " " -f 1`;
    echo "$i:$COUNT" >> /tmp/parameterCount.txt

done

echo "Frequency analysis saved to /tmp/parameterCount.txt ...";

egrep ":0$" /tmp/parameterCount.txt | cut -d ":" -f 1 > /tmp/deprecatedParameters.txt

echo "Potential deprecated parameter list saved to /tmp/deprecatedParameters.txt ..."
