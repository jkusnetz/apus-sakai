#!/bin/sh
#
# Remove a file from Confluence.  
#
# This script requires the Confluence Command Line interface:
#  http://confluence.atlassian.com/display/CONFEXT/Confluence+Command+Line+Interface
#
# This script only uploads a single file.  To upload a group of files, I use a for loop like: 
#
# for i in *; do /opt/installs/sakai/cafe-2.5.x/config-viewer/utils/remove_from_confluence.sh $i; done
#

if [ -z $JAVA_HOME ]; then
    echo "ERROR:  You must set your JAVA_HOME environment variable..."
    echo
    exit
fi

JAVA=$JAVA_HOME/bin/java

#JAR=/opt/confluence-soap-0.7/release/confluence-soap-0.7.jar
#JAR=/opt/confluence-cli-1.0.0/release/confluence-cli-1.0.0.jar
#JAR=/opt/confluence-cli-1.2.0/release/confluence-cli-1.2.0.jar
JAR=/opt/confluence-cli-1.3.0/release/confluence-cli-1.3.0.jar
if [ ! -e $JAR ]; then
    echo "ERROR: Can't find Confluence CLI..."
    echo
    exit
fi

SETTINGS_FILE=~/.confluence-cli

# You need a settings file to use this, with the following options set:
#
#  SERVER=http://confluence.sakaiproject.org/confluence
#  SPACE="~anthony.atkins@vt.edu"
#  PARENT="Home"

# The following settings are optional, if you don't have them, you'll be prompted to enter them:
#
#  CONFUSER=""
#  CONFPASS=""

if [ ! -s $SETTINGS_FILE ]; then
    echo "Cannot find local settings file $SETTINGS_FILE..."
    echo
    exit
else
    source $SETTINGS_FILE

    if [ -z "$1" ]; then
	echo "Usage: $0 [page to remove]"
	echo
	exit
    else
	# If there's no username, read it from the command line
	if [ -z "$CONFUSER" ]; then
	    read -p "Username: " CONFUSER
	fi
	
	# If there's no password, read it from the command line
	if [ -z "$CONFPASS" ]; then
	    read -s -p "Password:" CONFPASS
	    echo
	fi

	$JAVA -jar $JAR -v -s $SERVER -u $CONFUSER -p $CONFPASS --space $SPACE --title "$1" --action removePage
    fi
fi


