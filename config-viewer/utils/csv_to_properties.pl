#!/usr/bin/perl
#
# csv_to_properties.pl - created by duhrer@gmail.com on November 3, 2006
#
# This script converts a CSV file to a properties file.
#
# Copyright (c) 2007 University of the Highlands and Islands
# Copyright (c) 2006, 2007 Virginia Polytechnic Institute and State University
# 
# A copy of the Educational Community License has been included in this 
# distribution and is available at: http://www.opensource.org/licenses/ecl1.php
#
# Contributors:
# Anthony Atkins (duhrer@gmail.com)

unless ($ARGV[0] && $ARGV[1] && $ARGV[2]) { 
    die("Usage: csv_to_properties.pl {input_file} {properties_file} {messages_file}\n");
}

$input_file = $ARGV[0];
$properties_file = $ARGV[1];
$messages_file = $ARGV[2];

open (INPUT, $input_file) or die("Can't read input file $input_file...\n");
open (PROPERTIES, ">$properties_file") or die("Can't write to properties file $properties_file...\n");
open (MESSAGES, ">$messages_file") or die("Can't write to messages file $messages_file...\n");
$record_number = 0;

$header = "# Copyright (c) 2006,2007 Virginia Polytechnic Institute and State University\n" .
          "# Copyright (c) 2007,2008 UHI Millenium Institute\n" .
          "#\n" .
          "# A copy of the Educational Community License has been included in this\n" .
          "# distribution and is available at: http://www.opensource.org/licenses/ecl1.php\n" .
          "#\n" .
          "# Contributors:\n" .
          "# Anthony Atkins (duhrer@gmail.com)\n" .
          "#\n";


print PROPERTIES $header;
print PROPERTIES "# syntax message strings are now contained in a separate file:\n";
print PROPERTIES "#  tool/src/java/org/sakaiproject/configviewer/tool/bundle/propertiesHelpMessages.properties\n";
print PROPERTIES "#\n";
print PROPERTIES "# Automatically generated using csv_to_properties on " . localtime() . "...\n\n";


print MESSAGES $header;
print MESSAGES "# Automatically generated using csv_to_properties on " . localtime() . "...\n\n";

while (<INPUT>) {
    if ($record_number != 0) {
		chomp($_);
	
		# clean up idiosyncrasies with the Excel input
		$_ =~ s/^"//;
		$_ =~ s/"$//;
		$_ =~ s/\t"/\t/g;
		$_ =~ s/"\t/\t/g;
		$_ =~ s/""/"/g;
	
		($name, $type, $help, $tool, $version, $configurable, $rule, $qualifier, $nullable) = split("\t", $_);
		print PROPERTIES "\n";
		print PROPERTIES "properties.name.$record_number=$name\n";
		print PROPERTIES "properties.type.$record_number=$type\n";
		print PROPERTIES "properties.tool.$record_number=$tool\n";
		print PROPERTIES "properties.version.$record_number=$version\n";
		print PROPERTIES "properties.configurable.$record_number=$configurable\n";
		print PROPERTIES "properties.rule.$record_number=$rule\n";
		print PROPERTIES "properties.qualifier.$record_number=$qualifier\n";
		print PROPERTIES "properties.nullable.$record_number=$nullable\n";

		print MESSAGES "# help text for property $name\n";
		print MESSAGES "$name=$help\n\n";
    }

    $record_number++;
}

print PROPERTIES "\n\nproperties.count=" . ($record_number - 1) . "\n";

close(INPUT);
close(PROPERTIES);
close(MESSAGES);

print "Properties saved to $properties_file...\n\n";
print "Messages saved to $messages_file...\n\n";
