/**********************************************************************************
 * $URL:  $
 * $Id:  $
 ***********************************************************************************
 *
 * Copyright (c) 2007 The Sakai Foundation.
 * 
 * Licensed under the Educational Community License, Version 1.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at
 * 
 *      http://www.opensource.org/licenses/ecl1.php
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 *
 **********************************************************************************/
package org.sakaiproject.scorm.model.api;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;

import org.sakaiproject.scorm.exceptions.ResourceNotFoundException;

public abstract class ContentPackageResource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String path;

	private long length;

	private long lastModified;

	public ContentPackageResource() {
	}

	public ContentPackageResource(String path) {
		this.path = path;
		this.length = -1;
		this.lastModified = new Date().getTime();
	}

	public abstract InputStream getInputStream() throws ResourceNotFoundException;

	public long getLastModified() {
		return lastModified;
	}

	public long getLength() {
		return length;
	}

	public abstract String getMimeType();

	public String getPath() {
		return path;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public void setPath(String path) {
		this.path = path;
	}

}
