/**********************************************************************************
 * $URL: https://svn.rsmart.com/svn/vendor/branches/sakai/2.8.1.x/tool/tool-tool/su/src/java/org/sakaiproject/tool/su/SuTool.java $
 * $Id: SuTool.java 24915 2011-02-09 15:37:56Z jbush $
 ***********************************************************************************
 *
 * Copyright (c) 2005, 2006, 2007, 2008 The Sakai Foundation
 *
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.osedu.org/licenses/ECL-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 **********************************************************************************/

package org.sakaiproject.tool.su;

import java.util.Vector;
import java.util.Properties;
import java.util.Collection;
import java.text.MessageFormat;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sakaiproject.authz.api.*;
import org.sakaiproject.component.api.ServerConfigurationService;
import org.sakaiproject.event.api.Event;
import org.sakaiproject.event.api.EventTrackingService;
import org.sakaiproject.event.api.UsageSessionService;
import org.sakaiproject.tool.api.Session;
import org.sakaiproject.tool.api.SessionManager;
import org.sakaiproject.tool.api.ToolManager;
import org.sakaiproject.user.api.User;
import org.sakaiproject.user.api.UserDirectoryService;
import org.sakaiproject.user.api.UserNotDefinedException;
import org.sakaiproject.util.ResourceLoader;
import org.sakaiproject.util.StringUtil;
import org.sakaiproject.site.api.SiteService;
import org.sakaiproject.site.api.Site;
import org.sakaiproject.entity.api.Reference;
import org.sakaiproject.entity.cover.EntityManager;
import org.sakaiproject.authz.cover.FunctionManager;

/**
 * @author zach.thomas@txstate.edu
 */
public class SuTool
{
	private static final long serialVersionUID = 1L;

    final String SU_REALM_PROP_KEY = "su.realm";//set this in sakai.properties to the realm name with the roles and members who can su
    final String SU_REALM_CONFIG_PROPERTY_DEFAULT_NAME = "su.realm.default"; /// or this will be taken from the tool config file
    //final String SU_FUNCTION_CAN_SU_ANY = "su.function.can_su.any"; /// this security function in a role allows anyone to su
    final String SU_FUNCTION_CAN_SU = "su.can_su"; /// this security function in a role allows anyone to su
    static final String DEFAULT_SU_REALM_NAME = "!su.can_su_realm"; // default realm name if not specified in sakai.properties or as tool config property

	/** Our log (commons). */
	private static Log M_log = LogFactory.getLog(SuTool.class);

	protected static final String SU_BECOME_USER = "su.become";
	protected static final String SU_VIEW_USER = "su.view";

	ResourceLoader msgs = new ResourceLoader("tool-tool-su");

	// Service instance variables
	private AuthzGroupService M_authzGroupService = org.sakaiproject.authz.cover.AuthzGroupService
			.getInstance();

	private UserDirectoryService M_uds = org.sakaiproject.user.cover.UserDirectoryService.getInstance();

	private SecurityService M_security = org.sakaiproject.authz.cover.SecurityService.getInstance();

	private SessionManager M_session = org.sakaiproject.tool.cover.SessionManager.getInstance();

	private ServerConfigurationService M_config = org.sakaiproject.component.cover.ServerConfigurationService
			.getInstance();

    private SiteService M_site = org.sakaiproject.site.cover.SiteService.getInstance();

    private ToolManager M_tool = org.sakaiproject.tool.cover.ToolManager.getInstance();
    private AuthzGroupService M_authz = org.sakaiproject.authz.cover.AuthzGroupService.getInstance();


	private EventTrackingService M_event_service = org.sakaiproject.event.cover.EventTrackingService.getInstance();

	// getters for these vars
	private String username;

	private String validatedUserId;
	
	private String validatedUserEid;

	private User userinfo;

	private boolean allowed = false;

	// internal only vars
	private String message = "";

	private boolean confirm = false;

    private Site thisSite = null;

    private String suRealm = null;

    // base constructor
	public SuTool()
	{
        Collection registered = FunctionManager.getRegisteredFunctions("su");
        if (!registered.contains(SU_FUNCTION_CAN_SU)) {
             FunctionManager.registerFunction(SU_FUNCTION_CAN_SU);
        }
	}

	/**
	 * Functions
	 */
	public String su()
	{
		// SAK-620
		// Assuming input is KerberosName
		String userInputId = username;
        boolean useSuRealm = M_config.getBoolean("use.su.realm", false);
        /*
		String userEid = UcdEnterpriseUserService.getPersonEid(username);

		if(null==userEid){ //check by federated method
			userEid = UcdEnterpriseUserService.getFederatedPersonEid(username);
		}

		// We only switch user names for provided users
		if(null != userEid && !userEid.equals("")) {
		        M_log.info("Switch kerberosName to userEid : kerberosName = " + username + " : userEid = " + userEid);
		        setUsername(userEid);
		}
        */

		Session sakaiSession = M_session.getCurrentSession();
		FacesContext fc = FacesContext.getCurrentInstance();
		userinfo = null;
		message = "";

        if (useSuRealm && checkRealm(fc))
        {
            return "error";
        }

        try
		{
			// try with the user id
			userinfo = M_uds.getUser(username.trim());
			validatedUserId = userinfo.getId();
			validatedUserEid = userinfo.getEid();
		}
		catch (UserNotDefinedException e)
		{
			try
			{
				// try with the user eid
				userinfo = M_uds.getUserByEid(username.trim());
				validatedUserId = userinfo.getId();
				validatedUserEid = userinfo.getEid();
			}
			catch (UserNotDefinedException ee)
			{
				message = msgs.getString("no_such_user") + ": " + username;
				fc.addMessage("su", new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message + ":" + ee));
				M_log.warn("[SuTool] Exception: " + message);
				confirm = false;
				return "error";
			}
		}
		
		// don't try to become yourself
		if (sakaiSession.getUserEid().equals(validatedUserEid)) {
			confirm = false;
			message = msgs.getFormattedMessage("already_that_user", new Object[] {username});
			fc.addMessage("su", new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
			M_log.warn("[SuTool] Exception: " + message);
			confirm = false;
			return "error";
		}

        if (!confirm)
		{
			message = msgs.getString("displaying_info_for") + ": " + validatedUserEid;
			fc.addMessage("su", new FacesMessage(FacesMessage.SEVERITY_INFO, message, message + ":" + userinfo.getDisplayName()));
			Event event = M_event_service.newEvent(SU_VIEW_USER, M_uds.userReference(validatedUserId), false);
			M_event_service.post(event);
            // SAK-620, SAK-710
			// Setting user name back to userInputId to not confuse the user
			setUsername(userInputId);
			return "unconfirmed";
		}

		if (!useSuRealm && !getAllowed())
		{
			confirm = false;

			return "unauthorized";
        } else if (!useSuRealm && getAllowed()) {		
			confirm = true;

		} else if ( !getAllowedWithSuRealms()){
            confirm = false;

            // SAK-620
            // Setting user name back to userInputId to not confuse the user
            setUsername(userInputId);

            return "unauthorized";
        }




		// set the session user from the value supplied in the form
      message = "Username " + sakaiSession.getUserEid() + " becoming " + validatedUserEid;
		M_log.info("[SuTool] " + message);
      message = msgs.getString("title");
		fc.addMessage("su", new FacesMessage(FacesMessage.SEVERITY_INFO, message, message + ": "
				+ userinfo.getDisplayName()));
		
		// while keeping the official usage session under the real user id, switch over everything else to be the SU'ed user
		// Modeled on UsageSession's logout() and login()
		
		// Post an event
		Event event = M_event_service.newEvent(SU_BECOME_USER, M_uds.userReference(validatedUserId), false);
		M_event_service.post(event);

		// logout - clear, but do not invalidate, preserve the usage session's current session
		Vector saveAttributes = new Vector();
		saveAttributes.add(UsageSessionService.USAGE_SESSION_KEY);
		saveAttributes.add(UsageSessionService.SAKAI_CSRF_SESSION_ATTRIBUTE);
		sakaiSession.clearExcept(saveAttributes);
		
		// login - set the user id and eid into session, and refresh this user's authz information
		sakaiSession.setUserId(validatedUserId);
		sakaiSession.setUserEid(validatedUserEid);
		M_authzGroupService.refreshUser(validatedUserId);

		return "redirect";
	}

    private boolean checkRealm(FacesContext fc) {
        Properties m = M_tool.getCurrentTool().getRegisteredConfig();

        // load from sakai.properties, then tool config, then fall back to hard coded default
        if(null == suRealm){
            suRealm = StringUtil.trimToNull(M_config.getString(SU_REALM_PROP_KEY));
            if(null == suRealm){
                suRealm = m.getProperty(SU_REALM_CONFIG_PROPERTY_DEFAULT_NAME);
            }
            if(null == suRealm){
                suRealm = DEFAULT_SU_REALM_NAME;
            }
        }


        // look for and create realm if necessary

        AuthzGroup g = null;
        try {
            g = M_authz.getAuthzGroup(suRealm);
        }catch (GroupNotDefinedException e){ //realms isn't defined yet... so let's try
            if(!M_security.isSuperUser()){
                M_security.pushAdvisor( new SecurityAdvisor(){

                    Log log = LogFactory.getLog(this.getClass());

                    public SecurityAdvice isAllowed(String userId, String function, String reference) {
                        SecurityAdvice rv = SecurityAdvice.PASS;
                        Reference ref = EntityManager.newReference(reference);

                        if(userId.equals(M_session.getCurrentSessionUserId())
                                && function.equals(AuthzGroupService.SECURE_UPDATE_AUTHZ_GROUP)
                                && suRealm.equals(ref.getId())){

                            rv = SecurityAdvice.ALLOWED;
                        } else {
                            rv = SecurityAdvice.PASS;
                        }
                        log.warn(MessageFormat.format("SecurityAdvisor added to thread to allow realm creation queried with triplet: \n"
                                + "\tuser: {0}; function: {1}; reference: {2} --> {3}",userId, function, reference, rv.toString()));
                        return rv;
                    }

                });

            }

            M_log.warn("SU authzGroup (realm) not found: " + suRealm + "\n Attempting to create...");
            try{
                g = M_authz.addAuthzGroup(suRealm);
                M_authz.save(g);
                g = M_authz.getAuthzGroup(suRealm);
                M_log.warn("SU authzGroup (realm) created: " + suRealm);
            } catch (GroupAlreadyDefinedException ee){ //weirdness
                M_log.error("Conflicting exceptions from authzgroupservice: GroupNotDefinedException and GroupAlreadyDefinedException:"
                        + suRealm);
                e.printStackTrace();
                ee.printStackTrace();
            }
            catch (GroupNotDefinedException ee) { //something about the save didn't work
                M_log.error("Could not create su realm:"	+ suRealm);
                ee.printStackTrace();
            } catch (AuthzPermissionException ee) { // More weirdness: this is why we defined a SecurityAdvisor
                M_log.warn("Could not save (newly created) su realm:"	+ suRealm);
                M_log.warn("current user: permission denied");
            } catch (GroupIdInvalidException ee) { // something about the id ain't right
                M_log.warn("invalid realm ID: "+suRealm);
            }
        }
        M_security.clearAdvisors();

        if(null==g){
			message = MessageFormat.format(msgs.getString("configuration_error"), suRealm);
            fc.addMessage("su", new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
            M_log.warn("[SuTool] Exception: " + message);
            confirm = false;
            return true;
        }

        return false;
    }

    // simple way to support 2 buttons that do almost the same thing
	public String confirm()
	{
		confirm = true;
		return su();
	}

	/**
	 * Specialized Getters
	 */
	public boolean getAllowed()
	{
		Session sakaiSession = M_session.getCurrentSession();
		FacesContext fc = FacesContext.getCurrentInstance();

        if (!M_security.isSuperUser())
		{
			message = msgs.getString("unauthorized") + " " + sakaiSession.getUserId();
			M_log.error("[SuTool] Fatal Error: " + message);
			fc.addMessage("allowed", new FacesMessage(FacesMessage.SEVERITY_FATAL, message, message));
			allowed = false;
		}
		else
		{
			allowed = true;
		}
        return allowed;
	}

    public boolean getAllowedWithSuRealms(){
        Session sakaiSession = M_session.getCurrentSession();
        FacesContext fc = FacesContext.getCurrentInstance();

        /// noone can su a superuser
		if(M_security.isSuperUser(validatedUserId))
		{
			message = msgs.getString("unauthorized") + " " + sakaiSession.getUserId();
			M_log.error("[SuTool] Fatal Error: " + message);
			fc.addMessage("allowed", new FacesMessage(FacesMessage.SEVERITY_FATAL, message, message));
			allowed = false;
		}
		else if (M_security.isSuperUser())  // but an su can su anyone else
		{
			allowed = true;
		}
		else
		{

			String thisUserId = M_uds.getCurrentUser().getId();
			allowed = M_authz.isAllowed(thisUserId, SU_FUNCTION_CAN_SU, suRealm);


			if(!allowed){
				message = "Unauthorized user attempted access: " + sakaiSession.getUserId();
				M_log.error("[SuTool] Fatal Error: " + message);
				fc.addMessage("allowed", new FacesMessage(FacesMessage.SEVERITY_FATAL, message, message + ":" + " unauthorized"));
			}
		}


		return allowed;
    }

	/**
	 * Basic Getters and setters
	 */
	public String getUsername()
	{
		return username;
	}

	public String getPortalUrl()
	{
		return M_config.getPortalUrl();
	}

	public void setUsername(String username)
	{
		this.username = username.trim();
	}

	public User getUserinfo()
	{
		return userinfo;
	}

	public void setUserinfo(User userinfo)
	{
		this.userinfo = userinfo;
	}

}
