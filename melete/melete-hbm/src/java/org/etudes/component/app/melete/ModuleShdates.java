/**********************************************************************************
 *
 * $URL: https://svn.rsmart.com/svn/vendor/branches/foothills/melete/2.8.1.x/melete-hbm/src/java/org/etudes/component/app/melete/ModuleShdates.java $
 * $Id: ModuleShdates.java 21326 2010-06-23 23:34:34Z jbush $  
 ***********************************************************************************
 *
 * Copyright (c) 2008 Etudes, Inc.
 *
 * Portions completed before September 1, 2008 Copyright (c) 2004, 2005, 2006, 2007, 2008 Foothill College, ETUDES Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You may
 * obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 *
 **********************************************************************************/
package org.etudes.component.app.melete;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;

import org.etudes.api.app.melete.*;

/** @author Hibernate CodeGenerator */
public class ModuleShdates implements Serializable,ModuleShdatesService {

    /** identifier field */
    private Integer moduleId;

    /** nullable persistent field */
    private Date startDate;

    /** nullable persistent field */
    private Date endDate;

    /** nullable persistent field */
    private int version;

    /** identifier field */
    private org.etudes.component.app.melete.Module module;

    /** full constructor */
    public ModuleShdates(Date startDate, Date endDate, int version, org.etudes.component.app.melete.Module module) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.version = version;
        this.module = module;
    }

    /** Custom constructor */
    public ModuleShdates(Date startDate, Date endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /** default constructor */
    public ModuleShdates() {
    }

    /** copy constructor */
    public ModuleShdates(ModuleShdates oldModuleShdates)
    {
        this.startDate = oldModuleShdates.getStartDate();
        this.endDate = oldModuleShdates.getEndDate();
        this.module = null;
    }


    public Integer getModuleId() {
        return this.moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }


    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public org.etudes.api.app.melete.ModuleObjService getModule() {
        return this.module;
    }

    public void setModule(org.etudes.api.app.melete.ModuleObjService module) {
        this.module = (Module) module;
    }


    public String toString() {
        return new ToStringBuilder(this)
            .append("moduleId", getModuleId())
            .toString();
    }

}
