<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>

	<h:panelGrid id="uploadView1" columns="1" width="100%" styleClass="left">
		<h:column>
			<h:outputText id="uploadText1" value="#{msgs.contentuploadview_resourceHelper}" rendered="#{editSectionPage.shouldRenderResourceHelper}"/>				
			<h:outputText id="extraspacesUpload" value="    " styleClass="ExtraPaddingClass" />	
			<h:outputText id="uploadText2" value="#{editSectionPage.secResourceName}" rendered="#{editSectionPage.secResourceName != null}" styleClass="bold"/>	
			<h:outputText id="uploadText3" value="#{msgs.contentresourceHelperview_nofile}" rendered="#{editSectionPage.secResourceName == null}" styleClass="bold"/>	
			<h:outputText id="extraspacesUpload1" value="" styleClass="ExtraPaddingClass" />
			<h:commandLink id="serverViewButton"  action="#{editSectionPage.gotoResourceHelperView}" >
					<h:graphicImage id="replaceImg2" value="images/replace2.gif" styleClass="AuthImgClass"/>
					<h:outputText value="#{msgs.contentresourceHelperview_replace}" />
             </h:commandLink>	
          </h:column>      
</h:panelGrid>
		
		
	