<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<!--
 ***********************************************************************************
 * $URL: https://source.sakaiproject.org/contrib/etudes/melete/tags/2.6.0/melete-app/src/webapp/melete/header.jsp $
 * $Id: header.jsp 56408 2008-12-19 21:16:52Z rashmi@etudes.org $  
 ***********************************************************************************
 *
 * Copyright (c) 2008 Etudes, Inc.
 *
 * Portions completed before September 1, 2008 Copyright (c) 2004, 2005, 2006, 2007, 2008 Foothill College, ETUDES Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 *
 **********************************************************************************
-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="rtbc004.css">
<title>Header</title>
<script type="text/javascript" src="/library/editor/FCKeditor/fckeditor.js"></script>
</head>

<body marginwidth="0" marginheight="0" topmargin="0" leftmargin="0" bottommargin="0" rightmargin="0">
<div align="left">
<!-- This Begins the Top Table Area -->
  <table width="100%" height="45" border="0" cellpadding="0" cellspacing="0" class= "table1" >
    <tr valign="bottom">
      <td width="3%">&nbsp;      </td>      
      <td width="88%" valign="middle"><p class="style1">Course Name<br><span class="italics">Instructor</span></p></td>
      <td width="9%" valign="top" ><div align="left" class="style1">Logout</div></td>
    </tr>
	   </table>
<!-- This Ends the Top Table Area -->
</div>

<!-- This Begins the 2nd Table/Black Bar Area -->
    <table width="100%" class="toptable2" >
      <tr>
      <td colspan="2" class="toptable2">&nbsp; </td>
      </tr>
    </table>
<!-- This Ends the 2nd Table/Black Bar Area -->
</body>
</html>
