package com.rsmart.tool.melete;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;
import java.io.File;
import java.util.Enumeration;
import java.util.Map;
import java.util.HashMap;

import com.oreilly.servlet.MultipartRequest;

/**
 * Created by IntelliJ IDEA.
 * User: jbush
 * Date: Dec 29, 2009
 * Time: 12:32:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class MultipartWrapper extends HttpServletRequestWrapper {
    MultipartRequest mreq = null;

    public MultipartWrapper(HttpServletRequest req, String dir, int max_bytes)
                                       throws IOException {
      super(req);
      mreq = new MultipartRequest(req, dir, max_bytes);
    }

    // Methods to replace HSR methods
     public Enumeration getParameterNames() {
       return mreq.getParameterNames();
     }
     public String getParameter(String name) {
       return mreq.getParameter(name);
     }
     public String[] getParameterValues(String name) {
       return mreq.getParameterValues(name);
     }
     public Map getParameterMap() {
       Map map = new HashMap();
       Enumeration enumer = getParameterNames();

       while (enumer.hasMoreElements()) {
         String name = (String) enumer.nextElement();
         map.put(name, mreq.getParameterValues(name));
       }
       return map;
     }

     // Methods only in MultipartRequest
     public Enumeration getFileNames() {
       return mreq.getFileNames();
     }
     public String getFilesystemName(String name) {
       return mreq.getFilesystemName(name);
     }
     public String getContentType(String name) {
       return mreq.getContentType(name);
     }
     public File getFile(String name) {
       return mreq.getFile(name);
     }
   }
