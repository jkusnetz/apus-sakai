package com.rsmart.tool.melete;

import org.sakaiproject.component.cover.ServerConfigurationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.*;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: jbush
 * Date: Dec 29, 2009
 * Time: 12:22:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class MultipartFilter extends com.oreilly.servlet.MultipartFilter {
    private String dir = null;
    private int maxUploadSize = 1024 * 1024; //in bytes
    private static Log logger = LogFactory.getLog(MultipartFilter.class);

    public void init(FilterConfig config) throws ServletException {
        super.init(config);
        String meleteUploadDir = ServerConfigurationService.getString("melete.uploadDir",
                ServerConfigurationService.getSakaiHomePath() + "/melete/uploadDir");
        if (meleteUploadDir != null && meleteUploadDir.length() > 0) {
            dir = meleteUploadDir;
        }

        String max_file_size_mb = ServerConfigurationService.getString("content.upload.max", "1");
        try {
            maxUploadSize = Integer.parseInt(max_file_size_mb) * 1024 * 1024;
        }
        catch (Exception e) {
            // if unable to parse an integer from the value
            // in the properties file, use 1 MB as a default
            maxUploadSize = 1024 * 1024;
            logger.warn("setting upload size to default of ["  + maxUploadSize + "] bytes because of the following: " +
                    e.getMessage(), e);
        }
        logger.info("setting upload limit to [ " + maxUploadSize + "] bytes");
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String type = req.getHeader("Content-Type");

        // If this is not a multipart/form-data request continue
        if (type == null || !type.startsWith("multipart/form-data")) {
            chain.doFilter(request, response);
        } else {
            MultipartWrapper multi = new MultipartWrapper(req, dir, maxUploadSize);
            chain.doFilter(multi, response);
        }
    }
}
