package com.rsmart.sakaiproject.user.tool;

import org.sakaiproject.authz.api.AuthzGroup;
import org.sakaiproject.authz.cover.AuthzGroupService;
import org.sakaiproject.event.api.SessionState;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Adds support for the user type field to be a drop-down on the create/edit screens
 * <p/>
 * Created by IntelliJ IDEA.
 * User: jbush
 * Date: Sep 18, 2006
 * Time: 9:57:15 AM
 * To change this template use File | Settings | File Templates.
 */
public class UsersAction extends org.sakaiproject.user.tool.UsersAction {
   private static final String USER_TEMPLATE_PREFIX = "!user.template.";

   protected String buildEditContext(SessionState state, org.sakaiproject.cheftool.Context context) {
      context.put("types", getUserTypes());
      return super.buildEditContext(state, context);
   }

   /**
    * Build the context for the new user mode.
    */
   protected String buildNewContext(SessionState state, org.sakaiproject.cheftool.Context context) {
      context.put("types", getUserTypes());
      return super.buildNewContext(state, context);
   } // buildNewContext

   /**
    * Determine user types by looking at realms that start with "!user.template."
    * Doesn't include sample or maintain type, also puts "registered" type first in list
    *
    * @return list of user types in the system
    */
   protected List getUserTypes() {
      List userTypes = new ArrayList();
      List groups = AuthzGroupService.getInstance().getAuthzGroups(USER_TEMPLATE_PREFIX, null);
      //userTypes.add("registered");
      for (Iterator i = groups.iterator(); i.hasNext();) {
         AuthzGroup group = (AuthzGroup) i.next();
         String type = group.getId().replaceFirst(USER_TEMPLATE_PREFIX, "");
         if (!type.equals("sample")) {
            userTypes.add(type);
         }
      }
      return userTypes;
   }

}
