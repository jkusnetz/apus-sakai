/********************************************************************************** 
 * $URL: https://source.sakaiproject.org/contrib/dashboard/tags/1.0.2_RC05/impl/src/java/org/sakaiproject/dash/dao/mapper/SourceTypeMapper.java $ 
 * $Id: SourceTypeMapper.java 80458 2012-06-21 18:34:17Z jimeng@umich.edu $ 
 *********************************************************************************** 
 * 
 * Copyright (c) 2011 The Sakai Foundation 
 * 
 * Licensed under the Educational Community License, Version 2.0 (the "License"); 
 * you may not use this file except in compliance with the License. 
 * You may obtain a copy of the License at 
 * 
 * http://www.osedu.org/licenses/ECL-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
 * See the License for the specific language governing permissions and 
 * limitations under the License. 
 * 
 **********************************************************************************/ 

package org.sakaiproject.dash.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.sf.json.JSONArray;

import org.sakaiproject.dash.model.SourceType;
import org.springframework.jdbc.core.RowMapper;

/**
 * 
 *
 */
public class SourceTypeMapper implements RowMapper {

	/* (non-Javadoc)
	 * @see org.springframework.jdbc.core.RowMapper#mapRow(java.sql.ResultSet, int)
	 */
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		// source_type
		SourceType sourceType = new SourceType();
		sourceType.setId(rs.getLong("type_id"));
		sourceType.setIdentifier(rs.getString("type_identifier"));
		
		return sourceType;
	}

}
