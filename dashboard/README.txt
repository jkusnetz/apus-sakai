People have been asking for the dashboard tool for a long time.  It was finally implemented thanks to the efforts of participants in a graduate course in the School of Information at The University of Michigan in Winter term 2011. 

The course, "Open Source Software Development", was organized by Chuck Severance and included contributions from people at various institutions that use Sakai, especially Charles Hedrick of Rutgers.  

The students primarily responsible for defining requirements and developing the initial design for the dashboard were Caitlin Holman and Nikola Collins.

Another group of students from the School of Information at The University of Michigan in Winter term 2011 evaluated the usability of the sakai portal and suggested improvements, including recommendations related to the dashboard proposal.  Those students were Sayan Bhattacharyya, Elliott Andrew Manzon, Rachael Shaney and Amelia Mowry.

Tiffany Chow and Shwetangi Savant assisted with usability and quality assurance testing during development of the dashboard project while working as interns for CTools during the Fall term of 2012.




